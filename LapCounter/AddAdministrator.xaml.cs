﻿using LapCounter.dal;
using LapCounter.dto;
using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LapCounter
{
    public partial class AddAdministrator : Page
    {
        public AddAdministrator()
        {
            InitializeComponent();
        }
        private bool validateTextBoxes()
        {
            if(this.administratorName.Text.Equals("") || this.administratorEmail.Text.Equals(""))
            {
                MessageManager messageManager = new MessageManager();
                messageManager.ShowMessage("Add Administrator Field Validation", "Please make sure to fill out all of the administrator fields.");
                return false;
            }
            return true;
        }
        public void BackToChooseRaceButtonClicked(object sender, RoutedEventArgs e)
        {
            LapCounterChooseRace lapCountChooseRace = new LapCounterChooseRace();
            this.NavigationService.Navigate(lapCountChooseRace);
        }
        public void AddAdministratorButtonClicked(object sender, RoutedEventArgs e)
        {
            bool validated = validateTextBoxes();
            if (!validated)
            {
                return;
            }

            Administrator administrator = new Administrator();
            administrator.AdministratorName = this.administratorName.Text;
            administrator.AdministratorEmail = this.administratorEmail.Text;

            AdministratorDAO administratorDAO = new AdministratorDAO();
            bool added = administratorDAO.AddAdministrator(administrator);
            if (!added)
            {
                MessageManager messageManager = new MessageManager();
                messageManager.ShowMessage("Error Adding Administrator", "There was a problem adding this administrator.  Please try again.");
                return;
            }
            LapCounterChooseRace lapCounterChooseRace = new LapCounterChooseRace();
            this.NavigationService.Navigate(lapCounterChooseRace);
        }
    }
}
