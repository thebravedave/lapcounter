﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RaceTracker
{
    public class Logger<T>
    {
        private readonly bool debugMode;
        private string className { get; }
        public Logger(bool debugMode)
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
            string configPath = Path.Combine(documentsPath, "LapCounter.log");
            if (File.Exists(configPath))
                File.Delete(configPath);
            ConfigPath = configPath;
            Type typeParameterType = typeof(T);
            className = typeParameterType.ToString();
            this.debugMode = debugMode;
        }
        public string ConfigPath { get; }

        public void WriteException(string message, [CallerMemberName]string propertyName = "")
        {
            string fullMessage = string.Format("{4} {0}.{1}:{2}{3}", className, propertyName, message, Environment.NewLine, DateTime.Now.ToString());
            System.Diagnostics.Debug.WriteLine(fullMessage);
            try
            {
                File.AppendAllText(ConfigPath, fullMessage);
            }
            catch (Exception exc) { }
        }

        public void Write(string message, [CallerMemberName]string propertyName = "")
        {
            string fullMessage = string.Format("{4} {0}.{1}:{2}{3}", className, propertyName, message, Environment.NewLine, DateTime.Now.ToString());
            System.Diagnostics.Debug.WriteLine(fullMessage);
            if (debugMode)
            {
                try
                {
                    File.AppendAllText(ConfigPath, fullMessage);
                }
                catch (Exception exc) { }
            }
        }
    }
}
