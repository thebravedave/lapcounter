﻿using LapCounter.dal;
using LapCounter.dto;
using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LapCounter
{
    public partial class EditRace : Page
    {
        public int _raceId;
        private Race _race;
        public EditRace(int raceId)
        {
            _raceId = raceId;
            InitializeComponent();

            RaceDAO raceDAO = new RaceDAO();
            List<Race> races = raceDAO.GetRaceByRaceId(raceId);
            if(races.Count > 0)
            {
                _race = races[0];
                this.DataContext = _race;
            }
        }
        public void BackToRacesPageButtonClicked(object sender, RoutedEventArgs e)
        {
            LapCounterChooseRace lapCountChooseRace = new LapCounterChooseRace();
            this.NavigationService.Navigate(lapCountChooseRace);
        }
        public void UpdateRaceButtonClicked(object sender, RoutedEventArgs e)
        {
            RaceDAO raceDAO = new RaceDAO();
            DateTime selectedDate = (DateTime) raceDate.SelectedDate;
            string raceDateString = selectedDate.ToString("yyyy/MM/dd");
            _race.RaceDate = raceDateString;

            bool updated = raceDAO.UpdateRace(_race);

            if (updated)
            {
                LapCounterChooseRace lapCountChooseRace = new LapCounterChooseRace();
                this.NavigationService.Navigate(lapCountChooseRace);
            }
            else
            {
                MessageManager messageManager = new MessageManager();
                messageManager.ShowMessage("Issue Updating Race", "There was a problem updating your Race.  This might be because the name and date are the same.  Please check your form and try again.");
            }

        }

    }
}
