﻿using Org.LLRP.LTK.LLRPV1;
using Org.LLRP.LTK.LLRPV1.DataType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RaceTracker
{
    public class LLRP : IReader
    {
        //
        // Notes on LLRP LTK API
        //
        // Based on the source code, the API calls can return 4 types of result:
        //
        //  (1) If parameters are not present, throw a null reference exception;
        //  (2) If the remote side sends an error packet back, return null with an explanatory out message string parameter
        //  (3) If the remote side times out or there is an internal error, return null with a null out message string parameter
        //  (4) If all is good, return the requested packet with a status of M_OK
        //

        DateTime lastRoSpec = DateTime.Now;
        // LLRP Constants
        public const int LLRP_CONNECT_TIMEOUT_MS = 5000;
        public const int LLRP_COMMAND_TIMEOUT_MS = 3000;
        public const int MY_RO_SPEC_ID = 123;

        // Member variables
        private LLRPClient reader;
        private Dictionary<string, DateTime> _tagLastSeen = new Dictionary<string, DateTime>();
        Dictionary<string, string> tags = new Dictionary<string, string>();
        // Connection management
        private Timer timer = new Timer();
        private int _timer_skip_count = 0;
        private bool _message_outstanding;
        private bool _bConnected;

        // Properties
        private int _iTagTimeoutSeconds;
        private int _iTransmitPower;
        private int _iTriggerReadTimeSeconds;
        private int _iDirectionSpacingTimeMs;
        private bool _bTriggeredReads;
        private bool _bDirectional;

        // Triggered reads
        private bool _bTriggered;
        private DateTime _time_of_trigger;

        // Direction events
        private string _current_direction;
        private DateTime _last_direction_event;

        // Reader capability and configuration
        private MSG_GET_READER_CAPABILITIES_RESPONSE _cap_rsp;
        private MSG_GET_READER_CONFIG_RESPONSE _config_rsp;

        // Tracing
        //private static BooleanSwitch driver_trace = new BooleanSwitch("DRIVER_TRACE", "Driver trace switch");

        // Events for IReader interface
        public event NewTagsDelegate NewTags;
        public event ConnectedDelegate Connected;
        //public event DeviceDiscoveryDelegate OnDeviceDiscovery;
        public event DisconnectedDelegate Disconnected;

        public LLRP(string name, long id, string portString, string deviceSettings)
        {
            ////Trace.WriteLineIf(driver_trace.Enabled, "*** DEBUG Reader Constructor");
            reader = new LLRPClient();
            timer.Interval = 100;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            //timer.Interval = 100;            
            _bConnected = false;

            this.name = name;
            this.id = id;
            this.portString = portString;
            this.deviceSettings = deviceSettings;

            // Convert string properties to more easily accessible values
            _iTagTimeoutSeconds = retrieveIntegerProperty(TAG_TIMEOUT_NAME, TAG_TIMEOUT_DEFAULT, 1, 3600);
            _iTransmitPower = retrieveIntegerProperty(TRANSMIT_POWER_NAME, TRANSMIT_POWER_DEFAULT, 1, 100);
            _iTriggerReadTimeSeconds = retrieveIntegerProperty(TRIGGER_READ_TIME_NAME, TRIGGER_READ_DEFAULT, 1, 3600);
            _iDirectionSpacingTimeMs = retrieveIntegerProperty(DIRECTION_SPACING_TIME_NAME, DIRECTION_SPACING_DEFAULT, 1, 10000);
            _bTriggeredReads = (retrieveProperty(TRIGGER_SENSOR_NAME) == TS_OPTION_GPI1);
            _bDirectional = (retrieveProperty(DIRECTION_SENSOR_NAME) == DS_OPTION_GPI1);

            // Triggered reads
            _bTriggered = false;

            // Direction events
            _current_direction = "None";            
        }

        #region Properties

        // Description
        public string Description
        {
            get { return "LLRP V1"; }
        }

        public bool ReaderConnected
        {
            get
            {
                if (reader == null || !reader.IsConnected)
                    return false;
                return true;
            }
        }

        #endregion Properties

        #region Methods

        // Connection management
        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            double secondsSinceLast = (DateTime.Now - lastRoSpec).TotalMilliseconds;
            if(secondsSinceLast > 1000)
            {
                System.Diagnostics.Debug.WriteLine("TOTAL MILLISECONDS EXCEEDED!!! " + secondsSinceLast);
            }
            lastRoSpec = DateTime.Now;
            if (reader == null)
                return;

            if (!ReaderConnected)
            {
                HandleLLRPError("LLRP reader not connected");
                return;
            }


            update_trigger_state();
            update_direction_state();
            if (!_message_outstanding)
            {
                if ((!_bTriggeredReads) || (_bTriggeredReads && _bTriggered))
                {
                    _timer_skip_count = 0;
                    _message_outstanding = true;
                    Start_RoSpec();
                }
            }
            else
            {
                _timer_skip_count++;
                if (_timer_skip_count == 200)
                {
                    HandleLLRPError("Reader silent for 20 seconds");
                    return;
                }
            }
        }

        // Trigger events last for a specified time
        private void update_trigger_state()
        {
            if (_bTriggeredReads && _bTriggered)
            {
                if (DateTime.Now.Subtract(_time_of_trigger).TotalSeconds > _iTriggerReadTimeSeconds)
                {
                    _bTriggered = false;
                }
            }
        }

        // Direction events expire after a period of silence
        private void update_direction_state()
        {
            if (_bDirectional)
            {
                if (_current_direction != "None")
                {
                    if (DateTime.Now.Subtract(_last_direction_event).TotalMilliseconds > _iDirectionSpacingTimeMs)
                    {
                        _current_direction = "None";
                    }
                }
            }
        }

        #region Start/Stop AutoMode

        // Start auto mode
        public void StartAutoMode()
        {
            _tagLastSeen.Clear();
            _message_outstanding = false;
            _timer_skip_count = 0;
            timer.Enabled = true;
        }

        // Stop auto mode
        public void StopAutoMode()
        {
            _tagLastSeen.Clear();
            timer.Enabled = false;
        }

        #endregion Start/Stop AutoMode

        #region Connect/Disconnect

        // Disconnect
        public void Disconnect()
        {
            //Trace.WriteLineIf(driver_trace.Enabled, "*** DEBUG Reader Got Disconnect");
            if (reader == null)
                return;
            try
            {
                reader.OnReaderEventNotification -= new delegateReaderEventNotification(reader_OnReaderEventNotification);
                reader.OnRoAccessReportReceived -= new delegateRoAccessReport(reader_OnRoAccessReportReceived);
                reader.OnKeepAlive -= new delegateKeepAlive(reader_OnKeepAlive);
            }
            catch
            {
                ;
            }

            try
            {
                timer.Enabled = false;
                reader.Close();
                reader = null;
            }
            catch (Exception)
            {
                ;
            }

            if (_bConnected && Disconnected != null)
            {
                Disconnected(this);
            }

            // Successful disconnect
            _bConnected = false;
        }

        // Connect to LLRP reader
        public void Connect()
        {
            try
            {
                ENUM_ConnectionAttemptStatusType status;
                bool bSuccess;

                //Trace.WriteLineIf(driver_trace.Enabled, "*** DEBUG Reader open attempt");
                if (reader == null)
                    reader = new LLRPClient();
                bool ret = reader.Open(get_IP_Address(), LLRP_CONNECT_TIMEOUT_MS, out status);

                // Establish the connection
                if (!ret || status != ENUM_ConnectionAttemptStatusType.Success)
                {
                    Connected?.Invoke(this, false, "Error in reader Open: Returned value " + ret + " with message " + status.ToString().Replace("_", " "));
                    return;
                }

                //Trace.WriteLineIf(driver_trace.Enabled, "*** DEBUG Reader open success");

                // Subscribe to reader event notification and ro access report
                reader.OnReaderEventNotification += new delegateReaderEventNotification(reader_OnReaderEventNotification);
                reader.OnRoAccessReportReceived += new delegateRoAccessReport(reader_OnRoAccessReportReceived);
                reader.OnKeepAlive += new delegateKeepAlive(reader_OnKeepAlive);
                //MSG_ENABLE_EVENTS_AND_REPORTS tempmsg = new MSG_ENABLE_EVENTS_AND_REPORTS();
                //MSG_ERROR_MESSAGE temperror = null;
                //reader.ENABLE_EVENTS_AND_REPORTS(tempmsg, out temperror, 9000);
                // Configure reader
                bSuccess = Delete_RoSpec();
                if (bSuccess)
                {
                    bSuccess = Add_RoSpec();
                }
                if (bSuccess)
                {
                    bSuccess = Enable_RoSpec();
                }
                if (bSuccess)
                {
                    bSuccess = Enable_Events_And_Reports();
                }
                if (bSuccess)
                {
                    bSuccess = Get_Reader_Capability();
                }
                if (bSuccess)
                {
                    bSuccess = Get_Reader_Config();
                }
                if (bSuccess)
                {
                    bSuccess = Set_Antenna_Power();
                }
                if (bSuccess)
                {
                    bSuccess = Register_For_Events();
                }
                var GPO1Prop = retrieveProperty(GPO1_NAME);
                if (bSuccess && (GPO1Prop == GPO_OPTION_HIGH || GPO1Prop == GPO_OPTION_LOW))
                {
                    bSuccess = Set_GPO_State(1, GPO1Prop == GPO_OPTION_HIGH);
                }
                var GPO2Prop = retrieveProperty(GPO2_NAME);
                if (bSuccess && (GPO2Prop == GPO_OPTION_HIGH || GPO2Prop == GPO_OPTION_LOW))
                {
                    bSuccess = Set_GPO_State(2, GPO1Prop == GPO_OPTION_HIGH);
                }
                if (bSuccess)
                {
                    Connected?.Invoke(this, true, string.Empty);
                    _bConnected = true;
                }
                else
                {
                    throw new Exception("Configuration failed..");
                }
            }
            catch (Exception exc)
            {
                Connected?.Invoke(this, false, exc.Message);
            }

        }

        // Respond to keepalive message
        private void reader_OnKeepAlive(MSG_KEEPALIVE keepalive_msg)
        {
            MSG_KEEPALIVE_ACK msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_KEEPALIVE_ACK();

            this.reader.KEEPALIVE_ACK(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);

            //Trace.WriteLineIf(driver_trace.Enabled, "****LLRP Driver Keep Alive:" + Environment.NewLine + msg + Environment.NewLine + msg_err);
        }

        #endregion Connect/Disconnect

        #region Messages Received From Reader

        // Return tag information
        private void OnNewTags(IReader reader, TagObservation[] tagObservations)
        {
            NewTags?.Invoke(reader, tagObservations);
        }

        // New tag message from reader
        void reader_OnRoAccessReportReceived(MSG_RO_ACCESS_REPORT msg)
        {
            //Trace.WriteLineIf(driver_trace.Enabled, "*******************************************reader_OnRoAccessReportReceived DEBUG New Report" + Environment.NewLine + msg);
            System.Diagnostics.Debug.WriteLine("reader_OnRoAccessReportReceived :" + DateTime.Now.ToString("HH:mm:ss.fff"));
            _message_outstanding = false;

            if (msg.TagReportData == null) return;

            try
            {
                List<TagObservation> tagObservations = new List<TagObservation>();
                System.Diagnostics.Debug.WriteLine("Got total tags:" + msg.TagReportData.Length);
                for (int i = 0; i < msg.TagReportData.Length; i++)
                {
                    if (msg.TagReportData[i].EPCParameter.Count > 0)
                    {
                        string epc;
                        int iAntenna, iRSSI;

                        // reports come in two flavors.  Get the right flavor
                        if (msg.TagReportData[i].EPCParameter[0].GetType() == typeof(PARAM_EPC_96))
                        {
                            epc = ((PARAM_EPC_96)(msg.TagReportData[i].EPCParameter[0])).EPC.ToHexString();
                        }
                        else
                        {
                            epc = ((PARAM_EPCData)(msg.TagReportData[i].EPCParameter[0])).EPC.ToHexString();

                        }
                        if (!tags.ContainsKey(epc))
                            tags[epc] = epc;
                        System.Diagnostics.Debug.WriteLine("Reader Name "+Name+". Got tag " + epc + ". Total Count=" + tags.Keys.Count);
                        iAntenna = msg.TagReportData[i].AntennaID.AntennaID;
                        iRSSI = msg.TagReportData[i].PeakRSSI.PeakRSSI;

                        string tag_observation = epc + ":" + iAntenna;
                        if (!tagRecentlySeen(tag_observation) && tagObservations.Count(p=>p.TagID==epc) == 0)
                        {
                            System.Diagnostics.Debug.WriteLine("Read EPC:" + epc+" and antenna="+iAntenna+"tagSeenCount="+msg.TagReportData[i].TagSeenCount);
                            _tagLastSeen[tag_observation] = System.DateTime.Now;
                            TagObservation tao = new TagObservation(epc);
                            tao.AntennaID = iAntenna;
                            tao.RSSI = iRSSI;
                            tao.Direction = _current_direction;
                            tagObservations.Add(tao);
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("Ignoring EPC:" + epc);
                        }
                    }
                }//for
                System.Diagnostics.Debug.WriteLine("OnNewTags count="+tagObservations.Count+" " + DateTime.Now.ToString("HH:mm:ss.fff"));

                OnNewTags(this, tagObservations.ToArray());
            }
            catch (Exception ex)
            {
                //Trace.WriteLineIf(driver_trace.Enabled, "LLRP Driver Error: Report parsing exception: " + ex.ToString());
            }
        }

        // Has this tag been observed recently
        private bool tagRecentlySeen(string tag)
        {
            if (!_tagLastSeen.ContainsKey(tag))
            {
                return false;
            }
            double totalSeconds = DateTime.Now.Subtract(_tagLastSeen[tag]).TotalSeconds;
            bool recSeen = totalSeconds < _iTagTimeoutSeconds;
            //System.Diagnostics.Debug.WriteLine("tag " + tag + " totalSec=" + totalSeconds + " recSeen=" + recSeen);
            return false;
            //return recSeen;
        }

        // New event message from reader
        void reader_OnReaderEventNotification(MSG_READER_EVENT_NOTIFICATION msg)
        {
            //Trace.WriteLineIf(driver_trace.Enabled, "****LLRP Driver Event Notify:" + Environment.NewLine + msg);

            if (msg.ReaderEventNotificationData.GPIEvent != null)
            {
                int port_number = msg.ReaderEventNotificationData.GPIEvent.GPIPortNumber;
                bool bAsserted = msg.ReaderEventNotificationData.GPIEvent.GPIEvent;
                // User has configured GPI1 as a trigger
                if (_bTriggeredReads)
                {
                    if (port_number == 1 && bAsserted)
                    {
                        _bTriggered = true;
                        _time_of_trigger = DateTime.Now;
                    }
                }
                // User has configured GPI1 as an inner light sensor and GPI2 as an outer light sensor
                if (_bDirectional)
                {
                    // Just look at beam break events
                    if (bAsserted)
                    {
                        // Track activity - we are trying to detect long periods of no activity to reset the direction flag;
                        _last_direction_event = DateTime.Now;
                        if (_current_direction == "None")
                        {
                            // Determine direction
                            if (port_number == 1)
                            {
                                // Inner sensor triggered - direction = OUT
                                _current_direction = Direction.Out;
                                // Clear all recent tag reads to start over
                                _tagLastSeen.Clear();
                            }
                            else if (port_number == 2)
                            {
                                // Outer sensor triggered - direction = IN
                                _current_direction = Direction.In;
                                // Clear all recent tag reads to start over
                                _tagLastSeen.Clear();
                            }
                        }
                    }
                }
            }
            else
            {
                if (_cap_rsp != null && _cap_rsp.GeneralDeviceCapabilities != null &&
                    _cap_rsp.GeneralDeviceCapabilities.DeviceManufacturerName == 26554)
                {
                    //Trace.WriteLineIf(driver_trace.Enabled, "****LLRP Driver Event Notify. Device Name is 26554:");
                    if (msg.ReaderEventNotificationData != null && msg.ReaderEventNotificationData.ROSpecEvent != null)
                    {
                        {
                            //Trace.WriteLineIf(driver_trace.Enabled, "****LLRP Driver Event Notify. Device Name is 26554. EventType:" + msg.ReaderEventNotificationData.ROSpecEvent.EventType);
                            if (msg.ReaderEventNotificationData.ROSpecEvent.EventType == ENUM_ROSpecEventType.End_Of_ROSpec)
                            {
                                //Trace.WriteLineIf(driver_trace.Enabled, "****LLRP Driver Event Notify. Device Name is 26554. Event Type is End_of_Rospec:");
                                _message_outstanding = false;
                            }
                        }
                    }
                }
            }
        }

        // Based on user configuration -  calculate transmit power index
        private ushort calculate_transmit_index()
        {
            // See whether transmit power is ascending or descending
            int nItems = _cap_rsp.RegulatoryCapabilities.UHFBandCapabilities.TransmitPowerLevelTableEntry.Length;
            int entry_1 = _cap_rsp.RegulatoryCapabilities.UHFBandCapabilities.TransmitPowerLevelTableEntry[0].TransmitPowerValue;
            int entry_n = _cap_rsp.RegulatoryCapabilities.UHFBandCapabilities.TransmitPowerLevelTableEntry[nItems - 1].TransmitPowerValue;
            bool bAscending = (entry_n > entry_1);
            // Return the correct index in the range 1..nItems
            int index = (int)((nItems - 1) * _iTransmitPower / 100.0);
            // Previous calculation returned 0..nItems - 1 so adjust by 1
            index += 1;

            if (bAscending)
            {
                return (ushort)index;
            }
            else
            {
                return (ushort)(nItems + 1 - index);
            }
        }

        #endregion Messages Received From Reader

        #region Antenna Specific Methods

        // Return the number of antennas
        public int GetNumAntennas()
        {
            // Lookup reader capabilities
            if (_cap_rsp != null)
            {
                return _cap_rsp.GeneralDeviceCapabilities.MaxNumberOfAntennaSupported;
            }
            return -1;
        }

        // Not implemented
        public string[] GetTagList(int iAntenna)
        {
            throw new NotImplementedException();
        }

        // Not implemented
        public int GetAttenuationPercent(int iAntenna)
        {
            throw new NotImplementedException();
        }

        // Not implemented
        public void SetAttenuationPercent(int iAntenna, int iPercent)
        {
            throw new NotImplementedException();
        }

        #endregion Antenna Specific Methods

        #region Driver Settings

        // Extract the TCP port from the port connection string
        private int get_port()
        {
            // Example string: 10.10.10.10,300
            string[] fields = portString.Split(new char[] { ',' });
            // TCP Port
            if (fields.Length >= 2)
            {
                return Int32.Parse(fields[1]);
            }
            // Port lookup failed
            return 0;
        }

        // Return IP address
        private string get_IP_Address()
        {
            string defaultIp = "0.0.0.0";

            if (portString == null)
                return defaultIp;

            // Example string: 10.10.10.10,300
            string[] fields = portString.Split(new char[] { ',' });
            // IP Address
            if (fields.Length >= 1)
            {
                return fields[0];
            }
            // Address lookup failed
            return defaultIp;
        }

        // COM port not used
        public string defaultComPortSettings()
        {
            return string.Empty;
        }

        // Default settings for network port
        public string defaultNetworkPortSettings()
        {
            return "10.10.10.10,DISABLED,DISABLED,DISABLED";
        }

        #endregion Driver Settings

        #region Message Methods

        // LLRP reader capabilities
        private bool Get_Reader_Capability()
        {
            MSG_GET_READER_CAPABILITIES msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_GET_READER_CAPABILITIES();
            msg.RequestedData = ENUM_GetReaderCapabilitiesRequestedData.All;
            _cap_rsp = this.reader.GET_READER_CAPABILITIES(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);
            string response = string.Empty;
            if (_cap_rsp != null)
            {
                return true;
            }
            else if (msg_err != null)
            {
                response = msg_err.ToString();
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
            else
            {
                response = "Command time out!";
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
        }

        // LLRP reader configuration
        private bool Get_Reader_Config()
        {
            MSG_GET_READER_CONFIG msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_GET_READER_CONFIG();
            msg.RequestedData = ENUM_GetReaderConfigRequestedData.All;
            _config_rsp = this.reader.GET_READER_CONFIG(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);
            string response = string.Empty;
            if (_config_rsp != null)
            {
                return true;
            }
            else if (msg_err != null)
            {
                response = msg_err.ToString();
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
            else
            {
                response = "Command time out!";
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
        }

        // Clear all reader operation specs
        private bool Delete_RoSpec()
        {
            MSG_DELETE_ROSPEC msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_DELETE_ROSPEC();
            msg.ROSpecID = 0;
            MSG_DELETE_ROSPEC_RESPONSE rsp = this.reader.DELETE_ROSPEC(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);
            string response = string.Empty;
            if (rsp != null)
            {
                return true;
            }
            else if (msg_err != null)
            {
                response = msg_err.ToString();
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
            else
            {
                response = "Command time out!";
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
        }

        // Add spec to perform a read on demand
        private bool Add_RoSpec()
        {
            MSG_ADD_ROSPEC msg;
            MSG_ERROR_MESSAGE msg_err;

            //
            // Trigger is null - read on demand
            // Antenna ID is 0 - all antennas are enabled
            // Report is generated when read is complete
            //
            msg = new MSG_ADD_ROSPEC();
            msg.ROSpec = new PARAM_ROSpec();
            msg.ROSpec.CurrentState = ENUM_ROSpecState.Disabled;
            msg.ROSpec.Priority = 0;
            msg.ROSpec.ROSpecID = MY_RO_SPEC_ID;
            // Trigger instructions
            msg.ROSpec.ROBoundarySpec = new PARAM_ROBoundarySpec();
            msg.ROSpec.ROBoundarySpec.ROSpecStartTrigger = new PARAM_ROSpecStartTrigger();
            msg.ROSpec.ROBoundarySpec.ROSpecStartTrigger.ROSpecStartTriggerType = ENUM_ROSpecStartTriggerType.Null;
            msg.ROSpec.ROBoundarySpec.ROSpecStopTrigger = new PARAM_ROSpecStopTrigger();
            msg.ROSpec.ROBoundarySpec.ROSpecStopTrigger.ROSpecStopTriggerType = ENUM_ROSpecStopTriggerType.Duration;
            msg.ROSpec.ROBoundarySpec.ROSpecStopTrigger.DurationTriggerValue = 5000;
            // Reporting instructions
            msg.ROSpec.ROReportSpec = new PARAM_ROReportSpec();
            msg.ROSpec.ROReportSpec.ROReportTrigger = ENUM_ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec;
            msg.ROSpec.ROReportSpec.N = 0;
            msg.ROSpec.ROReportSpec.TagReportContentSelector = new PARAM_TagReportContentSelector();
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableAccessSpecID = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableAntennaID = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableChannelIndex = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableFirstSeenTimestamp = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableInventoryParameterSpecID = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableLastSeenTimestamp = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnablePeakRSSI = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableROSpecID = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableSpecIndex = true;
            msg.ROSpec.ROReportSpec.TagReportContentSelector.EnableTagSeenCount = true;
            // Antenna inventory instructions
            msg.ROSpec.SpecParameter = new UNION_SpecParameter();
            PARAM_AISpec aiSpec = new PARAM_AISpec();
            aiSpec.AntennaIDs = new UInt16Array();
            aiSpec.AntennaIDs.Add(0);
            aiSpec.AISpecStopTrigger = new PARAM_AISpecStopTrigger();
            aiSpec.AISpecStopTrigger.AISpecStopTriggerType = ENUM_AISpecStopTriggerType.Duration;
            aiSpec.AISpecStopTrigger.DurationTrigger = 5000;
            aiSpec.InventoryParameterSpec = new PARAM_InventoryParameterSpec[] { new PARAM_InventoryParameterSpec() };
            aiSpec.InventoryParameterSpec[0].InventoryParameterSpecID = 0x4d2;
            aiSpec.InventoryParameterSpec[0].ProtocolID = ENUM_AirProtocols.EPCGlobalClass1Gen2;
            msg.ROSpec.SpecParameter.Add(aiSpec);
            // Send command
            //Trace.WriteLineIf(driver_trace.Enabled, Environment.NewLine + "*********DEBUG READER AddRoSpec, " + Environment.NewLine + msg.ToString());
            MSG_ADD_ROSPEC_RESPONSE rsp = this.reader.ADD_ROSPEC(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);
            string response = string.Empty;
            //Trace.WriteLineIf(driver_trace.Enabled, "Response: " + Environment.NewLine + rsp);
            if (rsp != null)
            {
                return true;
            }
            else if (msg_err != null)
            {
                response = msg_err.ToString();
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
            else
            {
                response = "Command time out!";
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
        }

        // Enable reader spec
        private bool Enable_RoSpec()
        {
            MSG_ENABLE_ROSPEC msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_ENABLE_ROSPEC();
            msg.ROSpecID = MY_RO_SPEC_ID;
            //Trace.WriteLineIf(driver_trace.Enabled, Environment.NewLine + "*********DEBUG READER EnableRoSpec. " + Environment.NewLine + msg.ToString());
            MSG_ENABLE_ROSPEC_RESPONSE rsp = this.reader.ENABLE_ROSPEC(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);
            string response = string.Empty;
            //Trace.WriteLineIf(driver_trace.Enabled, "Response: " + Environment.NewLine + rsp);
            if (rsp != null)
            {
                return true;
            }
            else if (msg_err != null)
            {
                response = msg_err.ToString();
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
            else
            {
                response = "Command time out!";
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
        }

        // Enable events and reprots
        private bool Enable_Events_And_Reports()
        {
            MSG_ENABLE_EVENTS_AND_REPORTS msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_ENABLE_EVENTS_AND_REPORTS();

            this.reader.ENABLE_EVENTS_AND_REPORTS(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);

            //Trace.WriteLineIf(driver_trace.Enabled, "****LLRP Driver Enable Events Reports:" + Environment.NewLine + msg + Environment.NewLine + msg_err);

            return true;
        }

        // Start a read operation
        private bool Start_RoSpec()
        {
            System.Diagnostics.Debug.WriteLine("Start_RO_Spec start :" + DateTime.Now.ToString("HH:mm:ss.fff"));
            MSG_START_ROSPEC msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_START_ROSPEC();
            msg.ROSpecID = MY_RO_SPEC_ID;

            //Trace.WriteLineIf(driver_trace.Enabled, Environment.NewLine + "*********DEBUG READER START_ROSPEC. " + Environment.NewLine + msg.ToString());

            MSG_START_ROSPEC_RESPONSE rsp = this.reader.START_ROSPEC(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);
            string response = string.Empty;
            //Trace.WriteLineIf(driver_trace.Enabled, " Response: " + Environment.NewLine + rsp);
            if (rsp != null)
            {
                return true;
            }
            else if (msg_err != null)
            {
                response = msg_err.ToString();
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
            else
            {
                response = "Command time out!";
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }            
        }

        // Send a configuration message to set antenna power
        private bool Set_Antenna_Power()
        {
            MSG_SET_READER_CONFIG msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_SET_READER_CONFIG();
            msg.ResetToFactoryDefault = false;
            msg.AntennaConfiguration = new PARAM_AntennaConfiguration[1];
            // Target all antennas
            msg.AntennaConfiguration[0] = new PARAM_AntennaConfiguration();
            msg.AntennaConfiguration[0].AntennaID = 0;
            msg.AntennaConfiguration[0].RFReceiver = null;
            // Empty set of protocol instructions
            msg.AntennaConfiguration[0].AirProtocolInventoryCommandSettings = new UNION_AirProtocolInventoryCommandSettings();
            msg.AntennaConfiguration[0].RFTransmitter = new PARAM_RFTransmitter();
            // Not interested in channel index or hop table so just copy them from existing configuration
            if (_config_rsp.AntennaConfiguration.Length > 0)
            {
                msg.AntennaConfiguration[0].RFTransmitter.ChannelIndex = _config_rsp.AntennaConfiguration[0].RFTransmitter.ChannelIndex;
                msg.AntennaConfiguration[0].RFTransmitter.HopTableID = _config_rsp.AntennaConfiguration[0].RFTransmitter.HopTableID;
            }
            else
            {
                msg.AntennaConfiguration[0].RFTransmitter.ChannelIndex = 0;
                msg.AntennaConfiguration[0].RFTransmitter.HopTableID = 0;
            }
            // Set the transmit power
            msg.AntennaConfiguration[0].RFTransmitter.TransmitPower = calculate_transmit_index();
            // Send command
            //Trace.WriteLineIf(driver_trace.Enabled, Environment.NewLine + "*********DEBUG READER SET_READER_CONFIG. " + Environment.NewLine + msg.ToString());
            MSG_SET_READER_CONFIG_RESPONSE rsp = this.reader.SET_READER_CONFIG(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);
            string response = string.Empty;
            //Trace.WriteLineIf(driver_trace.Enabled, " Response: " + Environment.NewLine + rsp);
            if (rsp != null)
            {
                return true;
            }
            else if (msg_err != null)
            {
                response = msg_err.ToString();
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
            else
            {
                response = "Command time out!";
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
        }

        // Register for GPIO events
        private bool Register_For_Events()
        {
            MSG_SET_READER_CONFIG msg;
            MSG_ERROR_MESSAGE msg_err;

            msg = new MSG_SET_READER_CONFIG();
            msg.ResetToFactoryDefault = false;
            // Enable GPIO notifications
            msg.ReaderEventNotificationSpec = new PARAM_ReaderEventNotificationSpec();
            msg.ReaderEventNotificationSpec.EventNotificationState = new PARAM_EventNotificationState[2];
            msg.ReaderEventNotificationSpec.EventNotificationState[0] = new PARAM_EventNotificationState();
            msg.ReaderEventNotificationSpec.EventNotificationState[0].EventType = ENUM_NotificationEventType.GPI_Event;
            msg.ReaderEventNotificationSpec.EventNotificationState[0].NotificationState = true;
            // Enable GPIO 1 and GPIO 2
            if (_cap_rsp.GeneralDeviceCapabilities.GPIOCapabilities.NumGPIs >= 2)
            {
                msg.GPIPortCurrentState = new PARAM_GPIPortCurrentState[2];
                msg.GPIPortCurrentState[0] = new PARAM_GPIPortCurrentState();
                msg.GPIPortCurrentState[0].GPIPortNum = 1;
                msg.GPIPortCurrentState[0].Config = true;
                msg.GPIPortCurrentState[1] = new PARAM_GPIPortCurrentState();
                msg.GPIPortCurrentState[1].GPIPortNum = 2;
                msg.GPIPortCurrentState[1].Config = true;
            }
            //Enable Other
            msg.ReaderEventNotificationSpec.EventNotificationState[1] = new PARAM_EventNotificationState();
            msg.ReaderEventNotificationSpec.EventNotificationState[1].EventType = ENUM_NotificationEventType.ROSpec_Event;
            msg.ReaderEventNotificationSpec.EventNotificationState[1].NotificationState = true;
            // Send command
            //Trace.WriteLineIf(driver_trace.Enabled, Environment.NewLine + "*********DEBUG READER AddRoSpec. " + Environment.NewLine + msg.ToString());
            MSG_SET_READER_CONFIG_RESPONSE rsp = this.reader.SET_READER_CONFIG(msg, out msg_err, LLRP_COMMAND_TIMEOUT_MS);
            string response = string.Empty;
            //Trace.WriteLineIf(driver_trace.Enabled, "Response: " + Environment.NewLine + rsp);
            if (rsp != null)
            {
                return true;
            }
            else if (msg_err != null)
            {
                response = msg_err.ToString();
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
            else
            {
                response = "Command time out!";
                HandleLLRPError("LLRP Error: " + response);
                return false;
            }
        }

        // Log an error and close the connection
        void HandleLLRPError(string errorMsg)
        {
            timer.Enabled = false;
            //Trace.WriteLineIf(driver_trace.Enabled, "*********DEBUG READER LLRP ERROR: " + Environment.NewLine + errorMsg);
            Disconnect();
        }

        #endregion Message Methods

        #region Properties

        public const int PROPERTY_TAG_TIMEOUT = 0;
        public const string TAG_TIMEOUT_NAME = "Tag Timeout(s)";
        public const string TAG_TIMEOUT_FIELDNAME = "TagTimeout";
        public const string TAG_TIMEOUT_DEFAULT = "60";
        public const int PROPERTY_TRANSMIT_POWER = 1;
        public const string TRANSMIT_POWER_NAME = "Transmit Power (1-100)";
        public const string TRANSMIT_POWER_FIELDNAME = "PW";
        public const string TRANSMIT_POWER_DEFAULT = "100";
        public const int PROPERTY_TRIGGER_SENSOR = 2;
        public const string TRIGGER_SENSOR_NAME = "Trigger Sensor";
        public const string TRIGGER_SENSOR_FIELDNAME = "TS";
        public const string TRIGGER_SENSOR_DEFAULT = "Always On";
        public const int PROPERTY_TRIGGER_READ_TIME = 3;
        public const string TRIGGER_READ_TIME_NAME = "Trigger Read Time(s)";
        public const string TRIGGER_READ_TIME_FIELDNAME = "TRT";
        public const string TRIGGER_READ_DEFAULT = "30";
        public const int PROPERTY_DIRECTION_SENSOR = 4;
        public const string DIRECTION_SENSOR_NAME = "Direction Sensor";
        public const string DIRECTION_SENSOR_FIELDNAME = "DS";
        public const string DIRECTION_SENSOR_DEFAULT = "None";
        public const int PROPERTY_DIRECTION_SPACING_TIME = 5;
        public const string DIRECTION_SPACING_TIME_NAME = "Direction Spacing Time(ms)";
        public const string DIRECTION_SPACING_TIME_FIELDNAME = "DST";
        public const string DIRECTION_SPACING_DEFAULT = "2000";
        public const int PROPERTY_GPO1 = 6;
        public const string GPO1_NAME = "GPO1 Default State";
        public const string GPO1_FIELDNAME = "GPO1";
        public const string GPO1_DEFAULT = GPO_OPTION_DO_NOTHING;
        public const int PROPERTY_GPO2 = 7;
        public const string GPO2_NAME = "GPO2 Default State";
        public const string GPO2_FIELDNAME = "GPO2";
        public const string GPO2_DEFAULT = GPO_OPTION_DO_NOTHING;

        //Options for GPO
        public const string GPO_OPTION_DO_NOTHING = "Do Nothing";
        public const string GPO_OPTION_LOW = "Low";
        public const string GPO_OPTION_HIGH = "High";
        // Options for Trigger Sensor
        public const string TS_OPTION_ALWAYS_ON = "Always On";
        public const string TS_OPTION_GPI1 = "GPI1";

        // Options for Direction Sensor
        public const string DS_OPTION_NONE = "None";
        public const string DS_OPTION_GPI1 = "GPI1::Inner GPI2::Outer";
        private readonly string name;
        private readonly long id;
        private string portString;
        private string deviceSettings;

        public string getDescription()
        {
            return Description;
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public long ID
        {
            get
            {
                return id;
            }
        }

        // User settable properties
        public int getNumProperties()
        {
            return 5;
        }

        // Property name
        public string getPropertyName(int iWhich)
        {
            switch (iWhich)
            {
                case PROPERTY_TAG_TIMEOUT:
                    return TAG_TIMEOUT_NAME;
                case PROPERTY_TRANSMIT_POWER:
                    return TRANSMIT_POWER_NAME;
                case PROPERTY_TRIGGER_SENSOR:
                    return TRIGGER_SENSOR_NAME;
                case PROPERTY_TRIGGER_READ_TIME:
                    return TRIGGER_READ_TIME_NAME;
                case PROPERTY_DIRECTION_SENSOR:
                    return DIRECTION_SENSOR_NAME;
                case PROPERTY_DIRECTION_SPACING_TIME:
                    return DIRECTION_SPACING_TIME_NAME;
                case PROPERTY_GPO1:
                    return GPO1_NAME;
                case PROPERTY_GPO2:
                    return GPO2_NAME;
            }
            return null;
        }

        // Property type
        public string getPropertyType(int iWhich)
        {
            switch (iWhich)
            {
                case PROPERTY_TAG_TIMEOUT:
                    return "INTEGER";
                case PROPERTY_TRANSMIT_POWER:
                    return "INTEGER";
                case PROPERTY_TRIGGER_SENSOR:
                    return "CHOICE(Always On|GPI1)";
                case PROPERTY_TRIGGER_READ_TIME:
                    return "INTEGER";
                case PROPERTY_DIRECTION_SENSOR:
                    return "CHOICE(None|GPI1::Inner GPI2::Outer)";
                case PROPERTY_DIRECTION_SPACING_TIME:
                    return "INTEGER";
                case PROPERTY_GPO1:
                    return string.Format("CHOICE({0}|{1}|{2})", GPO_OPTION_DO_NOTHING, GPO_OPTION_LOW, GPO_OPTION_HIGH);

            }
            return null;
        }

        // Property value
        public string retrieveProperty(string name)
        {
            switch (name)
            {
                case TAG_TIMEOUT_NAME:
                    return extractField(TAG_TIMEOUT_FIELDNAME);
                case TRANSMIT_POWER_NAME:
                    return extractField(TRANSMIT_POWER_FIELDNAME);
                case TRIGGER_SENSOR_NAME:
                    return extractField(TRIGGER_SENSOR_FIELDNAME);
                case TRIGGER_READ_TIME_NAME:
                    return extractField(TRIGGER_READ_TIME_FIELDNAME);
                case DIRECTION_SENSOR_NAME:
                    return extractField(DIRECTION_SENSOR_FIELDNAME);
                case DIRECTION_SPACING_TIME_NAME:
                    return extractField(DIRECTION_SPACING_TIME_FIELDNAME);
                case GPO1_NAME:
                    return extractField(GPO1_FIELDNAME);
                case GPO2_NAME:
                    return extractField(GPO2_FIELDNAME);
            }
            return null;
        }

        // Integer property value
        public int retrieveIntegerProperty(string name, string default_value, int min_value, int max_value)
        {
            int result;

            string szVal = retrieveProperty(name);
            if (String.IsNullOrEmpty(szVal) || !int.TryParse(szVal, out result))
            {
                result = Convert.ToInt32(default_value);
            }
            if (result < min_value)
            {
                result = min_value;
            }
            if (result > max_value)
            {
                result = max_value;
            }
            return result;
        }

        // Store property in the database
        public void storeProperty(string name, string val)
        {
            switch (name)
            {
                case TAG_TIMEOUT_NAME:
                    setField(TAG_TIMEOUT_FIELDNAME, val);
                    break;
                case TRANSMIT_POWER_NAME:
                    setField(TRANSMIT_POWER_FIELDNAME, val);
                    break;
                case TRIGGER_SENSOR_NAME:
                    setField(TRIGGER_SENSOR_FIELDNAME, val);
                    break;
                case TRIGGER_READ_TIME_NAME:
                    setField(TRIGGER_READ_TIME_FIELDNAME, val);
                    break;
                case DIRECTION_SENSOR_NAME:
                    setField(DIRECTION_SENSOR_FIELDNAME, val);
                    break;
                case DIRECTION_SPACING_TIME_NAME:
                    setField(DIRECTION_SPACING_TIME_FIELDNAME, val);
                    break;
            }
        }

        #region Field Stuff

        // Currently the GUI creates a new record with "Field1=|Field2=|Field3=" empty fields

        // Extract a field from device settings
        private string extractField(string name)
        {
            // Example string: Lighting=Bright|Exposure=17.0
            if (deviceSettings == null)
            {
                deviceSettings = String.Empty;
            }
            string[] fields = deviceSettings.Split('|');
            foreach (string f in fields)
            {
                string[] name_value = f.Split('=');
                if (name_value.Length == 2 && name_value[0].Equals(name))
                {
                    return name_value[1];
                }
            }
            return null;
        }

        // Store a field in device settings
        private void setField(string name, string val)
        {
            if (deviceSettings == null)
            {
                deviceSettings = String.Empty;
            }
            // Example string: Lighting=Bright|Exposure=17.0
            string[] fields = deviceSettings.Split('|');
            StringBuilder sb = new StringBuilder();
            foreach (string f in fields)
            {
                string[] name_value = f.Split('=');
                // Ignore the field being updated
                if (name_value.Length == 2 && name_value[0].Equals(name))
                {
                    continue;
                }
                // Ignore blank fields
                if (name_value.Length != 2)
                {
                    continue;
                }
                sb.Append(f);
                sb.Append("|");
            }
            sb.Append(name);
            sb.Append("=");
            sb.Append(val);
            deviceSettings = sb.ToString();
        }


        #endregion Field Stuff

        #endregion Properties

        #region Device Discovery & Lights

        // Not implemented
        public void StartDeviceDiscovery()
        {
            throw new NotImplementedException();
        }

        // Not implemented
        public void StopDeviceDiscovery()
        {
            throw new NotImplementedException();
        }

        #endregion Device Discovery & Lights

        #endregion Methods

        bool Set_GPO_State(ushort port, bool state)
        {
            MSG_ERROR_MESSAGE msg_err;
            MSG_SET_READER_CONFIG msg = new MSG_SET_READER_CONFIG();

            // Build the message
            msg.GPOWriteData = new PARAM_GPOWriteData[1];
            msg.GPOWriteData[0] = new PARAM_GPOWriteData();
            msg.GPOWriteData[0].GPOPortNumber = port;
            msg.GPOWriteData[0].GPOData = state;

            // Send the message
            MSG_SET_READER_CONFIG_RESPONSE rsp =
            reader.SET_READER_CONFIG(msg, out msg_err, 2000);
            bool success = false;
            if (rsp != null)
            {
                // Success
                Console.WriteLine(rsp.ToString());
                success = true;

            }
            else if (msg_err != null)
            {
                // Error
                //Trace.WriteLineIf(driver_trace.Enabled, "*** DEBUG Reader Set_GPO_State error:" + msg_err);
            }
            else
            {
                // Timeout
                //Trace.WriteLineIf(driver_trace.Enabled, "*** DEBUG Reader Set_GPO_State connection timeout");
            }
            return success;
        }

        public void SetGPO1(bool state, int seconds)
        {
            //Task.Factory.StartNew(async () =>
            //{
            Set_GPO_State(1, state);
            Task.Delay(1000 * seconds).ContinueWith(p =>
            {
                Set_GPO_State(1, !state);
            });
            //});
        }

        public void SetGPO2(bool state, int seconds)
        {
            Set_GPO_State(2, state);
            Task.Delay(1000 * seconds).ContinueWith(p =>
            {
                Set_GPO_State(2, !state);
            });
        }


    }//LLRP

    class Direction
    {
        public const string None = "None", In = "In", Out = "Out";
    }
}
