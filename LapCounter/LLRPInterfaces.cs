﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceTracker
{
    public interface IReader : IDevice
    {
        void StartAutoMode();
        void StopAutoMode();
        void SetGPO1(bool high, int seconds);
        void SetGPO2(bool high, int seconds);
        event NewTagsDelegate NewTags;

    }
    public delegate void NewTagsDelegate(IReader reader, TagObservation[] readTag);

    public interface IDevice
    {
        long ID { get; }
        string Name { get; }
        string Description { get; }
        void Connect();
        void Disconnect();
        event DisconnectedDelegate Disconnected;
        event ConnectedDelegate Connected;

    }
    public delegate void ConnectedDelegate(IDevice device, bool success, string reason);
    public delegate void DisconnectedDelegate(IDevice device);
    public delegate void DeviceDiscoveredDelegate(string name, string ip_address);

    public class TagObservation
    {
        public TagObservation()
        {
            Direction = "None";
        }

        // Constructor
        public TagObservation(string tagID)
        {
            TagID = tagID;
            AntennaID = -1;
            RSSI = 0;
            X = 0;
            Y = 0;
            Z = 0;
            PositionType = "Unknown";
            Direction = "None";
        }

        // Properties
        private string tagID = null;
        public string TagID
        {
            get
            {
                return tagID;
            }
            set
            {
                tagID = value.ToUpper();
            }
        }
        public int? SequenceNumber { get; set; }
        public int AntennaID { get; set; }
        public int RSSI { get; set; }
        public double? X { get; set; }
        public double? Y { get; set; }
        public int? Z { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public double? Altitude { get; set; }
        public int? NumSatellites { get; set; }
        public DateTimeOffset? LastGpsFix { get; set; }
        public int? GpsAccuracy { get; set; }
        public string ParentVTag { get; set; }
        public string NearestFixed { get; set; }
        public string VTagType { get; set; }
        public string PositionType { get; set; }
        public string CustomFieldsType { get; set; }
        public string PositionReportingMode { get; set; }

        public Dictionary<string, object> CustomFields { get; set; } = new Dictionary<string, object>();

        public string Direction { get; set; }

        public override string ToString()
        {
            return string.Format("TagID:{0},PositionReportingMode:{1},X:{2},Y:{3},Z:{4},Latitude:{5},Longitude:{6},NearestFixed:{7},VTagType:{8},ParentVTag:{9},PositionType:{10}",
                TagID, PositionReportingMode, X, Y, Z, Latitude, Longitude, NearestFixed, VTagType, ParentVTag, PositionType);
        }
    }
}
