﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dto
{
    public class Race
    {
        public int Id { get; set; }
        public string RaceName { get; set; }
        public string RaceDate { get; set; }
        public string RaceStartTime { get; set; }
        public string RaceEndTime { get; set; }
    }
}
