﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dto
{

    public class User : ObservableObject
    {
        public User() { }
        public User(int id, int raceId, string runnerFirstName, string runnerLastName, int runnerGrade, string parentFirstName, string parentLastName, string email, int laps, string vestNumber, string tagId)
        {
            Id = id;
            RaceId = raceId;
            RunnerFirstName = runnerFirstName;
            RunnerLastName = runnerLastName;
            RunnerGrade = runnerGrade;
            ParentFirstName = parentFirstName;
            ParentLastName = parentLastName;
            Email = email;
            Laps = laps;
            VestNumber = vestNumber;
            TagId = tagId;
        }
        private int id;
        public int Id
        {
            get => id;
            set { SetProperty(ref id, value); }
        }

        private int raceId;
        public int RaceId
        {
            get => raceId;
            set { SetProperty(ref raceId, value); }
        }

        private string runnerFirstName;
        public string RunnerFirstName
        {
            get => runnerFirstName;
            set { SetProperty(ref runnerFirstName, value); }
        }

        private string runnerLastName;
        public string RunnerLastName
        {
            get => runnerLastName;
            set { SetProperty(ref runnerLastName, value); }
        }

        private int runnerGrade;
        public int RunnerGrade
        {
            get => runnerGrade;
            set { SetProperty(ref runnerGrade, value); }
        }

        private string parentFirstName = string.Empty;
        public string ParentFirstName
        {
            get => parentFirstName;
            set { SetProperty(ref parentFirstName, value); }
        }

        private string parentLastName = string.Empty;
        public string ParentLastName
        {
            get => parentLastName;
            set { SetProperty(ref parentLastName, value); }
        }

        private string email = string.Empty;
        public string Email
        {
            get => email;
            set { SetProperty(ref email, value); }
        }

        private int laps;
        public int Laps
        {
            get => laps;
            set { SetProperty(ref laps, value); }
        }

        private string vestNumber = string.Empty;
        public string VestNumber
        {
            get => vestNumber;
            set { SetProperty(ref vestNumber, value); }
        }

        private string tagId = string.Empty;
        public string TagId
        {
            get => tagId;
            set { SetProperty(ref tagId, value); }
        }


    }
}
