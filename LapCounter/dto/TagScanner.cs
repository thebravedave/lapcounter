﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dto
{
    class TagScanner : Scanner
    {
        public string TagScannerIp { get; set; }
        public bool DeviceReachable { get; set; }
    }
}
