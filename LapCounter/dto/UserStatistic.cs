﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dto
{
    class UserStatistic
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int LapNumber { get; set; }
        public int RaceId { get; set; }
        public string LapTime { get; set; }
    }
}
