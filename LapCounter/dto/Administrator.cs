﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dto
{
    class Administrator
    {
        public int Id { get; set; }
        public string AdministratorName { get; set; }
        public string AdministratorEmail { get; set; }
    }
}
