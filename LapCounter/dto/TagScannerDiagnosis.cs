﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dto
{
    public class TagScannersDiagnosis
    {
        public bool AllScannersConnected { get; set; }
        public bool NoScannersConnected  { get; set; }
}
}
