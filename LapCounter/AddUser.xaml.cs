﻿using LapCounter.dal;
using LapCounter.managers;
using System;
using System.Windows;
using System.Windows.Controls;


namespace LapCounter
{
    public partial class AddUser : Page
    {
        private int _raceId;
        public AddUser(int raceId)
        {
            _raceId = raceId;
            InitializeComponent();

        }
        public void AddUserData()
        {
            try
            {
                string runnerFirstName = this.runnerFirstName.Text.Trim();
                string runnerLastName = this.runnerLastName.Text.Trim();
                string runnerGradeString = this.runnerGrade.Text.Trim();
                string parentLastName = this.parentLastName.Text.Trim();
                string parentFirstName = this.parentFirstName.Text.Trim();
                string email = this.email.Text.Trim();
                string vestNumber = this.vestNumber.Text.Trim();
                string tagId = this.tagId.Text.Trim().Replace("{","");
                tagId = tagId.Replace("}", "");

                bool validated = ValidateFields();
                if (!validated)
                {
                    return;
                }
                int runnerGrade = Int32.Parse(runnerGradeString);
                UserDAO userDAO = new UserDAO();
                userDAO.AddUser(_raceId, runnerLastName, runnerFirstName, runnerGrade, parentLastName, parentFirstName, email, vestNumber, tagId);

                LapCounterHome lapCounterHome = new LapCounterHome(_raceId);
                this.NavigationService.Navigate(lapCounterHome);
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc.Message);
                MessageManager messageManager = new MessageManager();
                messageManager.ShowMessage("Field Error", "There was a problem with your 'Runner Grade' field.  Make sure you enter a number value.");
               
            }
        

        }           
        public void BackToRaceHomeButtonClicked(object sender, RoutedEventArgs e)
        {
            LapCounterHome lapCountHome = new LapCounterHome(_raceId);
            this.NavigationService.Navigate(lapCountHome);
        }
        private bool ValidateFields()
        {
            if (this.runnerFirstName.Text.Equals("") || this.runnerLastName.Text.Equals("") || this.runnerGrade.Text.Equals("") || this.parentLastName.Text.Equals("") || this.parentFirstName.Text.Equals("") || this.email.Text.Equals("") || this.vestNumber.Text.Equals("") || this.tagId.Text.Equals(""))
            {
                MessageManager messageManager = new MessageManager();
                messageManager.ShowMessage("Edit User Field Validation", "Please make sure to fill out all of the runner fields");
                return false;
            }
            return true;
        }
        private void AddUserButtonClicked(object sender, RoutedEventArgs e)
        {
            AddUserData();

        }
    }
}
