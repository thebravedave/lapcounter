﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LapCounter.managers
{
    class MessageManager
    {
        public bool ShowMessage(string caption, string outputMessage)
        {
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Warning;

            // Display message box
            MessageBox.Show(outputMessage, "Issue With Import File", button, icon);

            return true;
        }
        public bool ShowChallengeMessage(string caption, string messageBoxText)
        {

            MessageBoxButton button = MessageBoxButton.YesNo;
            MessageBoxImage icon = MessageBoxImage.Warning;

            // Display message box
            MessageBoxResult result = MessageBox.Show(messageBoxText, caption, button, icon);

            // Process message box results
            bool returnResult = false;
            switch (result)
            {
                case MessageBoxResult.Yes:
                    returnResult = true;
                    break;
                case MessageBoxResult.No:
                    returnResult = false;
                    break;
            }
            return returnResult;


        }
    }  
}
