﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace LapCounter.managers
{
    class EmailManager
    {

        public bool SendSecureEmail(string host, string userName, string password, string from, string to, string subject, string body)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = host;
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(userName, password);

               MailMessage mm = new MailMessage(from, to, subject, body);
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }


        public bool SendSecureEmailWithAttachment(string host, string userName, string password, string from, string to, string subject, string body, string attachmentPath)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = host;
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(userName, password);
                MailMessage mm = new MailMessage(from, to, subject, body);

                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                mm.Attachments.Add(new Attachment(attachmentPath));

                client.Send(mm);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
