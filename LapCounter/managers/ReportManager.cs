﻿using LapCounter.dal;
using LapCounter.dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.managers
{
    class ReportManager
    {
        public bool GenerateAndSendOutIndividualParentReport(int raceId)
        {
            RaceDAO raceDAO = new RaceDAO();
            List<Race> races = raceDAO.GetRaceByRaceId(raceId);
            if (races.Count == 1)
            {
                Race race = races[0];
                //need to get all the users for this 
                //need to send out the runner statistical information to the parents from the user email address.
                UserDAO userDAO = new UserDAO();
                StatisticsDAO statisticsDAO = new StatisticsDAO();

                ObservableCollection<User> users = userDAO.GetUsersByRaceId(raceId);
               
                EmailManager emailManager = new EmailManager();
                foreach (User currentUser in users)
                {
                    //need to get statistical information for the current user and email the parent the stats.
                    List<UserStatistic> userStatistics = statisticsDAO.GetUserStatisticsByUserId(currentUser.Id);
                    string userStatString = ProcessUserStatistics(race, currentUser.Id, userStatistics);
                    string subject = "Your race statistics for the race: " + race.RaceName;
                    bool mailResult = emailManager.SendSecureEmail("smtp.gmail.com", "infinid.david.pugh@gmail.com", "11T3chno", "davidmatthewpugh@gmail.com", currentUser.Email, subject, userStatString);                 
                }
                return true;
            }
            return false;
        }
        public string ProcessUserStatistics(Race race, int userId, List<UserStatistic> userStatistics)
        {
            UserDAO userDAO = new UserDAO();
            ObservableCollection<User> users = userDAO.GetUsersByUserId(race.Id, userId);
            if(users.Count == 0)
            {
                return "";
            }
            User user = users[0];

            string userStatString = "";            
            if(userStatistics.Count <= 1)
            {
                userStatString += "Runner Name: " + user.RunnerFirstName + " " + user.RunnerLastName + "\n\n";
                userStatString += "Race start time:" + race.RaceStartTime + "\n";
                userStatString += "Race end time: " + race.RaceEndTime + "\n";
                userStatString += "Runner completed zero laps \n";
                return userStatString;
            }
            UserStatistic previousUserStatistic = null;
            userStatString += "Runner Name: " + user.RunnerFirstName + " " + user.RunnerLastName + "\n\n";
            userStatString += "Race start time:" + race.RaceStartTime + "\n";
            userStatString += "Race end time: " + race.RaceEndTime + "\n";
            for (int i = 0; i < userStatistics.Count; i++)
            {
                UserStatistic currentUserStatistic = userStatistics[i];
                userStatString += "Lap # " + (i+1) + ", Lap Time: " + LapTimeInSeconds(race, previousUserStatistic, currentUserStatistic) + " seconds" + "\n";
                previousUserStatistic = currentUserStatistic;
            }           
            return userStatString;
        }
   
        private string LapTimeInSeconds(Race race, UserStatistic previousUserStatistic, UserStatistic currentUserStatistics)
        {
            if (previousUserStatistic == null)
            {
                string raceStartTime = race.RaceStartTime;
                string currentLapTime = currentUserStatistics.LapTime;             
                DateTime lastLapTimeDateTime = DateTime.Parse(raceStartTime);
                DateTime currentLapTimeDateTime = DateTime.Parse(currentLapTime);
                string lapTime = (currentLapTimeDateTime - lastLapTimeDateTime).TotalSeconds.ToString();
                return lapTime;
            }
            else
            {
                string previousLapTime = previousUserStatistic.LapTime;
                string currentLapTime = currentUserStatistics.LapTime;
                DateTime lastLapTimeDateTime = DateTime.Parse(previousLapTime);
                DateTime currentLapTimeDateTime = DateTime.Parse(currentLapTime);
                string lapTime = (currentLapTimeDateTime - lastLapTimeDateTime).TotalSeconds.ToString();
                return lapTime;
            }
        }
        public bool GenerateAndSendOutAdministratorReport(int raceId)
        {
            RaceDAO raceDAO = new RaceDAO();
            List<Race> races = raceDAO.GetRaceByRaceId(raceId);
            if(races.Count == 0)
            {
                return false;
            }
            Race race = races[0];
            string report = GenerateWholeRaceReport(raceId);
            if(report == null)
            {
                return false;
            }
            string fileName = "";
            try
            {
                string raceDate = DateTime.Now.ToString("MM_dd_yyyy");
                //create path for file
                string path = LocationManager.DatabaseLocation;
                DirectoryInfo di = Directory.CreateDirectory(path);
                //create CSV file for attachment
                string timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString();
                fileName = LocationManager.DatabaseLocation + "\\" + race.RaceName + "_" + raceDate + timestamp + "_" + ".csv";
                System.IO.File.WriteAllText(fileName, report);
            }
            catch(Exception ex)
            {
                Console.WriteLine("can't write file");
            }
            string body = "Here is your report for the race: " + race.RaceName + ".  Attached to this email are the administrator report details.";
            EmailManager emailManager = new EmailManager();
            AdministratorDAO administratorDAO = new AdministratorDAO();
            List<Administrator> administrators = administratorDAO.GetAllAdministrators();
            string subject = "Your report for the race: " + race.RaceName;

            foreach(Administrator administrator in administrators)
            {
                bool mailResult = emailManager.SendSecureEmailWithAttachment("smtp.gmail.com", "infinid.david.pugh@gmail.com", "11T3chno", "davidmatthewpugh@gmail.com", administrator.AdministratorEmail, subject, body, fileName);
            }
            return true;
        }
        public string GenerateWholeRaceReport(int raceId)
        {
            RaceDAO raceDAO = new RaceDAO();
            List<Race> races = raceDAO.GetRaceByRaceId(raceId);
            if(races.Count == 0)
            {
                //then someone deleted the race and the data is no longer available in the database
                return null;
            }
            Race race = races[0];

            StatisticsDAO statisticsDAO = new StatisticsDAO();
            UserDAO userDAO = new UserDAO();
            ObservableCollection<User> users = userDAO.GetUsersByRaceId(raceId);

            if(users.Count == 0)
            {
                //then no users can be found for this race because the users have been cleared from the database and the report can't be generated.
                return null;
            }
            string report = "Child Last,Child First,Grade,Parent Last,Parent First,Parent Email,Vest Number,Tag Id,Start Race,End Race,Total Laps,Lap 1,Lap 2,Lap 3,Lap 4,Lap 5,Lap 6,Lap 7,Lap 8,Lap 9,Lap 10,Lap 11,Lap 12,Lap 13,Lap 14,Lap 15,Lap 16,Lap 17,Lap 18,Lap 19,Lap 20,Lap 21,Lap 22,Lap 23,Lap 24,Lap 25,Lap 26,Lap 27,Lap 28,Lap 29,Lap 30,Lap 31,Lap 32,Lap 33,Lap 34,Lap 35,Lap 36,Lap 37,Lap 38,Lap 39,Lap 40,Lap 41,Lap 42,Lap 43,Lap 44,Lap 45,Lap 46,Lap 47,Lap 48,Lap 49,Lap 50\n";
            foreach(User currentUser in users)
            {
                int numberLaps = currentUser.Laps;
                string reportCurrentLine = "";
                reportCurrentLine += currentUser.RunnerLastName + ",";
                reportCurrentLine += currentUser.RunnerFirstName + ",";
                reportCurrentLine += currentUser.RunnerGrade + ",";
                reportCurrentLine += currentUser.ParentLastName + ",";
                reportCurrentLine += currentUser.ParentFirstName + ",";
                reportCurrentLine += currentUser.Email + ",";
                reportCurrentLine += currentUser.VestNumber + ",";
                reportCurrentLine += currentUser.TagId + ",";

                List<UserStatistic> userStatistics = statisticsDAO.GetUserStatisticsByUserId(currentUser.Id);
                UserStatistic previousUserStatistic = null;

                if(userStatistics.Count > 1)
                {
                    //this next line should be the time the race started
                    reportCurrentLine += race.RaceStartTime + ",";                
                    //this next line should be the time the race ended
                    reportCurrentLine += race.RaceEndTime + ",";
                    //this next line should be the number of laps the runner did for the race
                    reportCurrentLine += currentUser.Laps + ",";
                }
                
                for(int i=0; i < userStatistics.Count; i++)
                {
                    UserStatistic currentLapStatistic = userStatistics[i];
                    string lapTime = LapTimeInSeconds(race, previousUserStatistic, currentLapStatistic);
                    reportCurrentLine += lapTime + ",";
                    previousUserStatistic = currentLapStatistic;
                }
                reportCurrentLine = reportCurrentLine.TrimEnd(',');
                report += reportCurrentLine + "\n";
            }
            return report;
        }
    }
}
