﻿using System;
using System.Data.SQLite;

namespace LapCounter.managers
{
    public class DBConnectionManager
    {

        public static int BusyTimeout { get; set; }
        public static SQLiteConnection conn;
        public static object instanceLock;

        static DBConnectionManager()
        {
            instanceLock = new object();
            BusyTimeout = Convert.ToInt32(TimeSpan.FromMinutes(2).TotalMilliseconds);
        }

        public static SQLiteConnection CreateConnection(string connectionString)
        {
            SQLiteConnection connection = new SQLiteConnection(connectionString);
            connection.Open();

            using (SQLiteCommand command = connection.CreateCommand())
            {
                command.CommandText = string.Format("PRAGMA busy_timeout={0}", BusyTimeout);
                command.ExecuteNonQuery();
            }
            return connection;
        }
        public static SQLiteConnection GetConnection(string connectionString)
        {
            if (conn == null)
            {
                SQLiteConnection connection = new SQLiteConnection(connectionString);
                connection.Open();

                using (SQLiteCommand command = connection.CreateCommand())
                {
                    command.CommandText = string.Format("PRAGMA busy_timeout={0}", BusyTimeout);
                    command.ExecuteNonQuery();
                }
                conn = connection;
            }
            return conn;
        }
    }
}
