﻿using LapCounter.dal;
using LapCounter.dto;
using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LapCounter
{
    public partial class EditUser : Page
    {
        public User user;
        public EditUser(User userPassed)
        {
            user = userPassed;
            this.DataContext = user;
            InitializeComponent();                    
        }
        public void BackToRaceHomeButtonClicked(object sender, RoutedEventArgs e)
        {
            LapCounterHome lapCountHome = new LapCounterHome(user.RaceId);
            this.NavigationService.Navigate(lapCountHome);
        }
        private void EditUserButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                bool validated = ValidateTextBoxes();
                if (validated == false)
                {
                    return;
                }
                string gradeLevelString = this.runnerGrade.Text;

                string tagId = this.tagId.Text.Replace("{", "");
                tagId = tagId.Replace("}", "");
                user.TagId = tagId;
                user.RunnerGrade = Int32.Parse(gradeLevelString);
                UserDAO userDAO = new UserDAO();
                userDAO.UpdateUser(user);
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc.Message);
                MessageManager messageManager = new MessageManager();
                messageManager.ShowMessage("Field Error", "There was a problem with your 'Runner Grade' field.  Make sure you enter a number value.");
                return;
            }
            NavigateToHome();
        }
        private bool ValidateTextBoxes()
        {
            if (this.runnerFirstName.Text.Equals("") || this.runnerLastName.Text.Equals("") || this.runnerGrade.Text.Equals("") || this.parentLastName.Text.Equals("") || this.parentFirstName.Text.Equals("") || this.email.Text.Equals("") || this.vestNumber.Text.Equals("") || this.tagId.Text.Equals(""))
            {
                MessageManager messageManager = new MessageManager();
                messageManager.ShowMessage("Edit Runner Field Validation", "Please make sure to fill out all of the runner fields");
                return false;
            }
            return true;
        }
  
        private void NavigateToHome()
        {
            LapCounterHome editUserPage = new LapCounterHome(user.RaceId);
            this.NavigationService.Navigate(editUserPage);
        }       
    }
}
