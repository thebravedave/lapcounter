﻿using LapCounter.dal;
using LapCounter.dto;
using LapCounter.managers;
using RaceTracker;
using RaceTracker.managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace LapCounter
{

    public partial class LapCounterHome : Page
    {
        private DateTime raceStartTime;
        private DateTime raceEndTime;
        private static bool raceEnded = false;
        private static bool raceStarted = false;
        private static ObservableCollection<User> _users = new ObservableCollection<User>();
        private List<TagScanner> _tagScanners = new List<TagScanner>();
        Dictionary<string, string> tags = new Dictionary<string, string>();
        private int _raceId = 1;
        private static int _staticRaceId = 1;
        static IDictionary<string, List<DateTime>> _raceInformation = new Dictionary<string, List<DateTime>>();
        Race _currentRace;
        int minReadGapSeconds = 60;
        int minSignalStrength = -150;

        // Gets or sets the CollectionViewSource
       // public CollectionViewSource ViewSource { get; set; }

        // Gets or sets the ObservableCollection
        public ObservableCollection<User> Collection { get; set; }

        Logger<LapCounterHome> logger;

        ICollectionView Source { get; set; }

        public LapCounterHome(int raceId)
        {
            logger = new Logger<LapCounterHome>(true);
            _users = new ObservableCollection<User>();

            this.Collection = _users;
          //  this.ViewSource = new CollectionViewSource();
          //  ViewSource.Source = this.Collection;

            _raceInformation = new Dictionary<string, List<DateTime>>();
            _raceId = raceId;
            _staticRaceId = raceId;
            InitializeComponent();

            LoadUserInformation(null);
            LoadScannerInformation(null);
            LoadRaceInformation();
            SetCurrentRaceTime();

            minReadGapSeconds = RaceTracker.Properties.Settings.Default.MinReadGap;
            minSignalStrength = RaceTracker.Properties.Settings.Default.MinSignalStrength;


            // Specify a sorting criteria for a particular column
            // ViewSource.SortDescriptions.Add(new SortDescription("Vest Number", ListSortDirection.Ascending));

            // Let the UI control refresh in order for changes to take place.
            //  ViewSource.View.Refresh();
        }

        private void CheckIfRaceReady()
        {
            TagScannersDiagnosis tagScannerDiagnosis = TestScanners();
            dgTagScanners.ItemsSource = _tagScanners;

            //RaceDAO raceDAO = new RaceDAO();
            //List<Race> races = raceDAO.GetRaceByRaceId(_raceId);

            // if (races.Count > 0)
            // {
            //Race race = races[0];
            Race race = getRace();
            //then the race has  ended so we should not enable the start stop, etc buttons
            if(!race.RaceStartTime.Equals("") && !race.RaceEndTime.Equals(""))
            {
                sendRaceDetailsToAdminButton.IsEnabled = true;
                sendRaceDetailsToRunnersButton.IsEnabled = true;
                startRaceButton.IsEnabled = false;
                stopRaceButton.IsEnabled = false;
                clearRaceButton.IsEnabled = true;
                importButton.IsEnabled = false;
                addUserButton.IsEnabled = false;
                removeUserButton.IsEnabled = false;
                editUserButton.IsEnabled = false;
                addTagScannersButton.IsEnabled = true;
                deleteTagScannerButton.IsEnabled = true;
                recheckTagScannersButton.IsEnabled = true;
                navigateToChooseRacesPageButton.IsEnabled = true;
                return;
            }
            if (!race.RaceStartTime.Equals(""))
            {
                //then the race has started but is still running (not completed yet)
                navigateToChooseRacesPageButton.IsEnabled = false;
                startRaceButton.IsEnabled = false;
                stopRaceButton.IsEnabled = true;
                clearRaceButton.IsEnabled = true;
                importButton.IsEnabled = false;
                addUserButton.IsEnabled = false;
                removeUserButton.IsEnabled = false;
                editUserButton.IsEnabled = false;
                addTagScannersButton.IsEnabled = false;
                deleteTagScannerButton.IsEnabled = false; 
                recheckTagScannersButton.IsEnabled = false;
                return;
            }

            //then this means the race hasn't started but there are also no pingable scanners so race can't start
            if (tagScannerDiagnosis.NoScannersConnected)
            {
                navigateToChooseRacesPageButton.IsEnabled = true;
                startRaceButton.IsEnabled = false;
                stopRaceButton.IsEnabled = false;
                clearRaceButton.IsEnabled = true;
                importButton.IsEnabled = true;
                addUserButton.IsEnabled = true;
                removeUserButton.IsEnabled = true;
                editUserButton.IsEnabled = true;
                addTagScannersButton.IsEnabled = true;
                deleteTagScannerButton.IsEnabled = true;
                recheckTagScannersButton.IsEnabled = true;
                return;
            }
            //then either there are no users or no tagScanners so we should not let the race start
            if (_users.Count == 0 || _tagScanners.Count == 0)
            {
                //then the racemaster has to fill out some details before the race can start.
                //disable the start button
                startRaceButton.IsEnabled = false;
                stopRaceButton.IsEnabled = false;
                clearRaceButton.IsEnabled = true;
                importButton.IsEnabled = true;
                addUserButton.IsEnabled = true;
                removeUserButton.IsEnabled = true;
                editUserButton.IsEnabled = true;
                addTagScannersButton.IsEnabled = true;
                deleteTagScannerButton.IsEnabled = true;
                recheckTagScannersButton.IsEnabled = true;
                return;
            }
            //then the race has not started and everything is ready for the race.
            navigateToChooseRacesPageButton.IsEnabled = true;
            startRaceButton.IsEnabled = true;
            stopRaceButton.IsEnabled = false;
            clearRaceButton.IsEnabled = true;
            importButton.IsEnabled = true;
            addUserButton.IsEnabled = true;
            removeUserButton.IsEnabled = true;
            editUserButton.IsEnabled = true;
            addTagScannersButton.IsEnabled = true;
            deleteTagScannerButton.IsEnabled = true;
            recheckTagScannersButton.IsEnabled = true;
            //}
        }
        private void SetCurrentRaceTime()
        {
            string raceTimeLabelString = "";
            if(!raceStarted)
            {
                //then the race has not started and we can return a formated string of zero
                raceTimeLabelString = "Race time:";
            }
            else if(raceStarted && raceEnded)
            {
                //that means the race is over and we should compare the race time to the difference 
                //between recorded start and recorded end
                TimeSpan timeSpan = raceEndTime - raceStartTime;
                int totalSeconds = Convert.ToInt32((raceEndTime - raceStartTime).TotalSeconds);
                raceTimeLabelString = "Race time: " + timeSpan.ToString(@"hh\:mm\:ss");
            }
            else
            {
                try
                {
                    //else we calculate diff now from raceStartTime
                    int toalSeconds = Convert.ToInt32((DateTime.Now - raceStartTime).TotalSeconds);
                    TimeSpan timeSpan = DateTime.Now - raceStartTime;
                    raceTimeLabelString = "Race time: " + timeSpan.ToString(@"hh\:mm\:ss");
                }
                catch(Exception exc)
                {
                    Console.WriteLine("conversion error!");
                }

            }
            logger.Write("******UPDATING RACE TIME*************" + raceTimeLabelString);
            raceTimeLabel.Dispatcher.BeginInvoke((Action)(() =>
            {
                raceTimeLabel.Content = raceTimeLabelString;
            })); 
        }

        private void SetLaps(ObservableCollection<User> users)
        {
            foreach(User user in users)
            {
                try { 
                    List<DateTime> currentUsersLaps = _raceInformation[user.TagId];
                    if(currentUsersLaps.Count < 2)
                    {
                        user.Laps = 0;
                    }
                    else
                    {
                        user.Laps = currentUsersLaps.Count - 1;
                    }
                }
                catch(Exception exc)
                {
                    user.Laps = 0;
                }
            }
        }
        private void LoadRaceInformation()
        {         
            Race race = getRace();
            raceNameLabel.Content = race.RaceName;

            //see if the race is completed and if so set the raceStart andraceEnd DateTime member variables
            if (!race.RaceStartTime.Equals("") && !race.RaceEndTime.Equals(""))
            {
                //then the race is ended
                raceEnded = true;
                raceStarted = true;
                raceStartTime = DateTime.Parse(race.RaceStartTime);
                raceEndTime = DateTime.Parse(race.RaceEndTime);
            }
            else if (!race.RaceStartTime.Equals("")){
                //then we are in the middle of the race!!!  :)
                raceStartTime = DateTime.Parse(race.RaceStartTime);
                raceEnded = false;
                raceStarted = true;
            };

            ObservableCollection<User> users = getUsers();
            if (!raceEnded)
            {
                SetLaps(users);
            }
            
            //todo need to set the number of laps from our raceInformation dictionary
            //dgUsers.ItemsSource = users;
            //dgUsers.Items.Refresh();

            //get tag scanners
            // _tagScanners = scannerDAO.GetTagScannersForRace(_raceId);
            //TestScanners();

            //TODO SHOULD I BE SETTING THE SCANNERS EVERY TIME I GET NEW TAGS INCOMING?
            //TODO SEEMS LIKE THE THING YOU MIGHT WANT TO DO JUST ONCE#################################
            //List<TagScanner> scanners = getTagScanners();
            //dgTagScanners.ItemsSource = scanners;
            //######################################################################3
         

            CheckIfRaceReady();

        }
        ObservableCollection<User> getUsers()
        {
            if(_users.Count  == 0)
            {
                UserDAO userDAO = new UserDAO();

                    _users = userDAO.FetchAllUsers(_raceId);                
            }
            return _users;
        }
        List<TagScanner> getTagScanners()
        {
            if(_tagScanners.Count == 0)
            {
                ScannerDAO scannerDAO = new ScannerDAO();
                _tagScanners = scannerDAO.GetTagScannersForRace(_raceId);
            }
            return _tagScanners;
        }
        private Race getRace()
        {
            if (_currentRace == null)
            {
                ScannerDAO scannerDAO = new ScannerDAO();
                RaceDAO raceDAO = new RaceDAO();
                List<Race> races = raceDAO.GetRaceByRaceId(_raceId);
                if (races.Count > 0)
                {
                    _currentRace = races[0];
                }
            }
            return _currentRace;

        }

        private void UpdateRaceTime()
        {
            //we will run resetting of the current race time to be run at a different thread which will end when the race does
            var bgw = new BackgroundWorker();
            bgw.DoWork += (_, __) =>
            {
                int myCount = 0;
                while(!raceEnded)
                {
                    logger.Write("UpdateRaceTime.DoWork");
                    SetCurrentRaceTime();
                    if (myCount == 60)
                    {
                        lock (ThreadingManager.instanceLock)
                        {
                            foreach(var user in _users)
                            {
                                logger.Write("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^User " + user.TagId + " Laps=" + user.Laps);
                            }
                        }
                        myCount = 0;
                    }
                    myCount++;
                    System.Threading.Thread.Sleep(1000);
                }
                logger.Write("UpdateRaceTime.DoWork Done");
            };
            bgw.RunWorkerAsync();
        }

        private void TurnOnWaitingSpinner()
        {
            logger.Write("TurnOnWaitingSpinner");
            waitingSpinner.Dispatcher.BeginInvoke((Action)(() =>
            {
                waitingSpinner.Visibility = Visibility.Visible;
                waitingSpinner.Spin = true;
                waitingSpinner.Opacity = 100;
            }));
        }

        private void TurnOffWaitingSpinner()
        {
            logger.Write("TurnOffWaitingSpinner");
            waitingSpinner.Dispatcher.BeginInvoke((Action)(() =>
            {
                waitingSpinner.Visibility = Visibility.Visible;
                waitingSpinner.Spin = false;
                waitingSpinner.Opacity = 0;
            }));
        }
        private void NavigateToChooseRacesPage(object sender, RoutedEventArgs e)
        {
            LapCounterChooseRace editUserPage = new LapCounterChooseRace();
            this.NavigationService.Navigate(editUserPage);
        }
        private void AddTagScannersButtonClicked(object sender, RoutedEventArgs e)
        {
            AddTagScanner addTagScanner = new AddTagScanner(_raceId);
            this.NavigationService.Navigate(addTagScanner);
        }
        private void RemoveTagScannersButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                TagScanner tagScanner = (TagScanner)dgTagScanners.SelectedItem;
                ScannerDAO scannerDAO = new ScannerDAO();
                scannerDAO.DeleteScanner(tagScanner.Id, _raceId);
                List<TagScanner> scanners = scannerDAO.GetTagScannersForRace(_raceId);
                LoadScannerInformation(scanners);
            }
            catch(Exception exc)
            {
                Console.WriteLine("test");
            }          
        }
        private void LoadScannerInformation(List<TagScanner> scanners)
        {
            if (scanners == null)
            {
                scanners = getTagScanners();
            }
            _tagScanners = scanners;
            dgTagScanners.ItemsSource = scanners;
            TestScanners();
            CheckIfRaceReady();

        }

        public TagScannersDiagnosis TestScanners()
        {
            List<TagScanner> newTagScanners = new List<TagScanner>();
            bool allScannersOffline = true;
            bool someScannersOffline = false;
            TagScannersDiagnosis tagScannersDiagnosis = new TagScannersDiagnosis();
            foreach (TagScanner currentTagScanner in _tagScanners)
            {
                bool pingable = false;
                Ping pinger = null;
                string ipAddress = currentTagScanner.TagScannerIp;
                try
                {
                    pinger = new Ping();
                    PingReply reply = pinger.Send(ipAddress);
                    pingable = reply.Status == IPStatus.Success;
                }
                catch (Exception exc)
                {
                    // Discard PingExceptions and return false;
                    Console.WriteLine("ping exception");
                }
                finally
                {
                    if (pinger != null)
                    {
                        pinger.Dispose();
                    }
                }
                if (pingable)
                {
                    //then this internal ip address is reachable.
                    // this.pingLabel.Content = "The ip address is reachable";
                    currentTagScanner.DeviceReachable = true;
                    allScannersOffline = false;
                }
                else
                {
                    //then this internal ip address is not reachable
                    // this.pingLabel.Content = "The ip address is not reachable.  This could be because the scanner is not connected to the same internal network as the Lap Counter software or that the scanner is broken.";
                    someScannersOffline = true;
                    currentTagScanner.DeviceReachable = false;
                }
                newTagScanners.Add(currentTagScanner);
            }
            if (allScannersOffline == true)
            {
                tagScannersDiagnosis.NoScannersConnected = true;
            }
            if (!someScannersOffline)
            {
                tagScannersDiagnosis.AllScannersConnected = true;
            }
            _tagScanners = newTagScanners;
            return tagScannersDiagnosis;
        }

        public void TestIpAddress(object sender, RoutedEventArgs e)
        {
            TestScanners();
            dgTagScanners.ItemsSource = _tagScanners;
            CheckIfRaceReady();
        }
        private void AddUserButtonClicked(object sender, RoutedEventArgs e)
        {          
            AddUser addUserPage = new AddUser(_raceId);
            this.NavigationService.Navigate(addUserPage);
        }
        private void RemoveUserButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                //make sure they really want to delete this user
                MessageManager messageManager = new MessageManager();
                bool result = messageManager.ShowChallengeMessage("Remove User", "Are you sure you want to delete this runner?");
                if (!result)
                {
                    return;
                }
               // User user = (User)dgUsers.SelectedItem;

                var selected = dgUsers.SelectedItems;
                UserDAO userDAO = new UserDAO();
                foreach (var item in selected)
                {
                    try
                    {
                        User user = item as User;
                        userDAO.DeleteUser(user.Id, _raceId);
                    }
                    catch(Exception exc)
                    {
                        Console.WriteLine("Race administrator tried to delete a user that doesn't exist.");
                    }



                }
                ObservableCollection<User> users = userDAO.GetUsersByRaceId(_raceId);
                LoadUserInformation(users);
            }
            catch(Exception exc)
            {
                return;
            }
            //LoadUserInformation(usersTest);
            //LoadRaceInformation();
        }
        private void LoadUserInformation(ObservableCollection<User> users)
        {
            if(users == null)
            {
                UserDAO userDAO = new UserDAO();
                _users = userDAO.GetUsersByRaceId(_raceId);
            }
            else
            {
                _users = users;
            }
            // dgUsers.ItemsSource = users;
            // dgUsers.Items.Refresh();

            logger.Write("***!!*** Updating LoadUserInformation");
            Source = CollectionViewSource.GetDefaultView(_users);

            dgUsers.ItemsSource = CollectionViewSource.GetDefaultView(this.Source);

            this.Source.SortDescriptions.Add(new SortDescription("VestNumber", ListSortDirection.Ascending));


            


        }
        private void EditUserButtonClicked(object sender, RoutedEventArgs e)
        {
            User user = dgUsers.SelectedItem as User;
            if (user == null)
                return;
            user.RaceId = _raceId;
            EditUser editUserPage = new EditUser(user);
            this.NavigationService.Navigate(editUserPage, user); 
        }
        private void SendRaceDetailsToAdmin(object sender, RoutedEventArgs e)
        {
            var bgw = new BackgroundWorker();
            bgw.DoWork += (_, __) =>
            {
                
                //going to disable the stop and clear buttons because we will be doing this work in the background
                SetButtonsState(false, false, false, false, false, false, false, false, false, false);
                TurnOnWaitingSpinner();
                
               

                ReportManager reportManager = new ReportManager();
                reportManager.GenerateAndSendOutAdministratorReport(_raceId);
                
            };
            bgw.RunWorkerCompleted += (_, __) =>
            {
                TurnOffWaitingSpinner();
                /*wasn't sure why we need to reload the race information so i commented this out
                Dispatcher.Invoke(() =>
                {
                    LoadRaceInformation();
                });
                */

            };
            bgw.RunWorkerAsync();
        }
        private void SendRaceDetailsToRunners(object sender, RoutedEventArgs e)
        {
            var bgw = new BackgroundWorker();
            bgw.DoWork += (_, __) =>
            {

                //going to disable the stop and clear buttons because we will be doing this work in the background
                SetButtonsState(false, false, false, false, false, false, false, false, false, false);
                TurnOnWaitingSpinner();
                
                ReportManager reportManager = new ReportManager();
                reportManager.GenerateAndSendOutIndividualParentReport(_raceId);

            };
            bgw.RunWorkerCompleted += (_, __) =>
            {
                TurnOffWaitingSpinner();
                Dispatcher.Invoke(() =>
                {
                    LoadRaceInformation();
                });
                

            };
            bgw.RunWorkerAsync();
        }
        private void ImportData(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                // Open document 
                List<int> lineErrors = new List<int>();
                List<string> lineErrorReason = new List<string>();
                int currentLineNumber = 1;
                string fileName = dlg.FileName;
                try
                {
                    UserDAO userDAO = new UserDAO();
                    const Int32 BufferSize = 128;
                    using (var fileStream = File.OpenRead(fileName))
                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
                    {
                        String line;
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            try
                            {
                                string[] words = line.Split(',');
                                if(words.Length != 8)
                                {
                                    string error = "Only has " + words.Length.ToString() + " fields";
                                    throw new Exception(error);
                                }
                                
                                string childLastName = words[0].Trim();
                                string childFirstName = words[1].Trim();
                                int childGrade = Int32.Parse(words[2].Trim());
                                string parentLastName = words[3].Trim();
                                string parentFirstName = words[4].Trim();
                                string email = words[5].Trim();                            
                                string vestNumber = words[6];    
                                string tagId = words[7].Trim();
                                
                                bool addUserResult = userDAO.AddUser(_raceId, childLastName, childFirstName, childGrade, parentLastName, parentFirstName, email, vestNumber, tagId);
                                Console.WriteLine(addUserResult);
                               
                            }
                            catch(Exception ex)
                            {
                                lineErrors.Add(currentLineNumber);
                                lineErrorReason.Add(ex.Message);
                            }
                            currentLineNumber++;
                        }                     
                    }
                }
                catch(Exception exc)
                {
                    Console.WriteLine("error");
                }
                if(lineErrors.Count > 0)
                {
                    string finalString = "";
                    for(int i = 0; i < lineErrors.Count; i++)
                    {
                        int lineNumber = lineErrors[i];
                        string lineError = lineErrorReason[i];
                        finalString += "Line # " + lineNumber.ToString() + ", Issue: " + lineErrorReason[i] + "{0}";

                    }
                    String outputMessage = string.Format(finalString, Environment.NewLine);
                    MessageManager messageManager = new MessageManager();
                    messageManager.ShowMessage("Issue With Import File", outputMessage);
                }
                LoadUserInformation(null);
            }
        }
        private void StartRace(object sender, RoutedEventArgs e)
        {
            MessageManager messageManager = new MessageManager();
            bool startRace = messageManager.ShowChallengeMessage("Verify Start of Race", "Are you sure you want to start the race?");
            if (!startRace)
            {
                return;
            }
            TagScannersDiagnosis tagScannersDiagnosis = TestScanners();

            if (!tagScannersDiagnosis.AllScannersConnected && !tagScannersDiagnosis.NoScannersConnected)
            {
                bool result1 = messageManager.ShowChallengeMessage("Some Tag Scanners Disconnected", "Some of the tag scanners are disconnected, are you sure you want to start the race?");
                if (!result1)
                {
                    return;
                }
            }
            SetButtonsState(false, false, false, false, true, false, false, false, false, false);
            raceStartTime = DateTime.Now;
            raceStarted = true;
            raceEnded = false;
            Console.WriteLine(raceStartTime);


            UserDAO userDAO = new UserDAO();
            ObservableCollection<User> users = userDAO.GetUsersByRaceId(_raceId);
            DateTime startTime = DateTime.Now;

            foreach (var user in users)
            {
                List<DateTime> currentDateTimeList = new List<DateTime>();
                currentDateTimeList.Add(startTime);
                _raceInformation.Add(user.TagId, currentDateTimeList);
            }

            Console.WriteLine("start");

            //List<Race> races = raceDAO.GetRaceByRaceId(_raceId);

            // if (races.Count == 1)
            // {
            //Race race = races[0];
            Race race = getRace();
            race.RaceStartTime = raceStartTime.ToString();
            RaceDAO raceDAO = new RaceDAO();
            raceDAO.UpdateRace(race);
           // }
            UpdateRaceTime();

            LoadUserInformation(_users);

            logger.Write("About to connect to gateways");
            vtagmanager.VTagManager.Instance.NewSecurityIndicationEvent += Instance_NewSecurityIndicationEvent;
            vtagmanager.VTagManager.Instance.ConnectionStatusEvent += Instance_ConnectionStatusEvent;
            //for each tag scanner we have to connect it
            foreach (TagScanner currentTagScanner in _tagScanners)
            {                
                string portString = currentTagScanner.TagScannerIp + ",DISABLED,DISABLED,DISABLED";
                string deviceSettings = "TagTimeout=1|ReaderMode=|ReaderPower=|DirectionMinRSSI=|ReaderSearchMode=";
                logger.Write("Connecting to Gateway "+currentTagScanner.TagScannerIp);
                vtagmanager.VTagManager.Instance.AddGateway(vtagmanager.VTagType.VTag, currentTagScanner.TagScannerIp, 8422, "admin", "change", currentTagScanner.TagScannerIp.ToString(), "LapCounter", false);
            }

        }

        private void Instance_ConnectionStatusEvent(vtagmanager.ConnectionStatus connectionStatus)
        {
            string actionMsg = connectionStatus.IsConnected ? "Connected" : "Disconnected";
            logger.Write("Instance_ConnectionStatusEvent "+actionMsg);
            MessageBox.Show(string.Format("Gateway {0} is {1}", connectionStatus.GatewayID, actionMsg));
        }

        private void Instance_NewSecurityIndicationEvent(vtagmanager.libs.messages.SecurityIndication securityIndication)
        {
            logger.Write("LapCounter new Tags from reader " + securityIndication.GatewayID + ". tag id=" + securityIndication.TagID + " and RSSI="+securityIndication.RSSI+ " and timestamp=" + DateTime.Now.ToString("HH:mm:ss.fff"));

            try
            {
                //the scanner senses an rfid tag
                //logger.Write("Got read from tag id:" + readTag.TagID);
                string tagId = securityIndication.TagID;

                if(securityIndication.RSSI < minSignalStrength)
                {
                    logger.Write("Tag ID "+tagId+" Signal Strength of "+securityIndication.RSSI+" not strong enough");
                    return;
                }

                if (!_raceInformation.ContainsKey(tagId)) {
                    logger.Write("RaceInformation does not countain tag id of " + tagId);
                    return;
                }

                if (!tags.ContainsKey(tagId))
                    tags[tagId] = tagId;

                //need to get the lap time list for this tag's user and add a new time to it
                //but only if the time is 45 seconds since the last tag
                logger.Write("Instance_NewSecurityIndicationEvent requesting lock for tag "+securityIndication.TagID);
                lock (ThreadingManager.instanceLock)
                {
                    logger.Write("Instance_NewSecurityIndicationEvent got lock for tag " + securityIndication.TagID);
                    List<DateTime> currentUserDateTimeLaps = _raceInformation[tagId];

                    DateTime lastLapTime = currentUserDateTimeLaps[currentUserDateTimeLaps.Count - 1];

                    DateTime currentTime = DateTime.Now;


                    var diffInSeconds = (currentTime - lastLapTime).TotalSeconds;

                    logger.Write("Instance_NewSecurityIndicationEvent diffInSeconds="+diffInSeconds+", lastLapTime= " + lastLapTime.ToString("HH:mm:ss:fff")+", currentTime="+currentTime.ToString("HH:mm:ss:fff"));

                    if (diffInSeconds < minReadGapSeconds)
                    {
                        //then there has been less than 45 seconds since the last time the tag was scanned so this is a duplicate and we can ignore
                        logger.Write("Tag ID " + tagId + " Seen too recently: "+diffInSeconds);
                        return;
                    }


                    currentUserDateTimeLaps.Add(currentTime);

                    //now we need to update our _users list with this new lap
                    foreach (var user in _users)
                    {
                        if (user.TagId.Equals(tagId))
                        {
                            //then this is our user and we need to increment their laps
                            //logger.Write("********************************************************************<<<<<<<<<<<< Updating User lap. Current Lap="+(currentUserDateTimeLaps.Count-1));
                            user.Laps = currentUserDateTimeLaps.Count - 1;
                            logger.Write("********************************************************************************<<<<<<<<<<<< "+securityIndication.TagID+", Updated User lap to " + (currentUserDateTimeLaps.Count - 1));
                        }
                    }
                }
                logger.Write("Instance_NewSecurityIndicationEvent out of LOCK");
                /*
                Dispatcher.Invoke(() =>
                {
                    LoadUserInformation(_users);
                    //LoadRaceInformation();
                });
                */
            }
            catch (Exception exc)
            {
                Console.WriteLine("error");
            }

        }

        private void ClearRace(object sender, RoutedEventArgs e)
        {
            logger.Write("************************ClearRace");
            var bgw = new BackgroundWorker();
            TurnOnWaitingSpinner();
            bgw.DoWork += (_, __) =>
            {
                MessageManager messageManager = new MessageManager();
                bool startRace = messageManager.ShowChallengeMessage("Verify Clearing of Race", "Are you sure you want to clear the race?  The runners in the race and the tag scanners will not be deleted but this will clear all the statistical data for this race and end the race.");               
                if (!startRace)
                {
                    return;
                }
                SetButtonsState(false, false, false, false, false, false, false, false, false, false);
                
                disconnectGateways();

                //need to delete the userstatistics information and the race start and race end times
                StatisticsDAO statisticsDAO = new StatisticsDAO();
                bool statsDeletedResult = statisticsDAO.DeleteStatisticsForRace(_raceId);

                raceStarted = false;
                raceEnded = false;
                SetCurrentRaceTime();

                //List<Race> races = raceDAO.GetRaceByRaceId(_raceId);
                //if (races.Count == 1)
                //{
                //Race race = races[0];
                Race race = getRace();
                race.RaceStartTime = "";
                race.RaceEndTime = "";
                RaceDAO raceDAO = new RaceDAO();
                raceDAO.UpdateRace(race);
                //}
                _currentRace.RaceStartTime = "";
                _currentRace.RaceEndTime = "";

                DateTime resetTime = DateTime.Now;
                raceStartTime = resetTime;
                raceEndTime = resetTime;
                //need to update all the users for this race and zero out their laps
                UserDAO userDAO = new UserDAO();
                ObservableCollection<User> users = userDAO.GetUsersByRaceId(_raceId);
                foreach (User currentUser in users)
                {
                    currentUser.Laps = 0;
                    userDAO.UpdateUser(currentUser);
                }     
            };
            _raceInformation = new Dictionary<string, List<DateTime>>();

            bgw.RunWorkerCompleted += (_, __) =>
            {
                TurnOffWaitingSpinner();
                Dispatcher.Invoke(() =>
                {
                    LoadUserInformation(null);
                    LoadRaceInformation();
                });

            };
            bgw.RunWorkerAsync();

        }
        private void StopRace(object sender, RoutedEventArgs e)
        {
            logger.Write("************************StopRace");
            var bgw = new BackgroundWorker();
            bgw.DoWork += (_, __) =>
            {
                MessageManager messageManager = new MessageManager();
                bool startRace = messageManager.ShowChallengeMessage("Verify End of Race", "Are you sure you want to end the race?");
                if (!startRace)
                {
                    return;
                }
                vtagmanager.VTagManager.Instance.NewSecurityIndicationEvent -= Instance_NewSecurityIndicationEvent;
                vtagmanager.VTagManager.Instance.ConnectionStatusEvent -= Instance_ConnectionStatusEvent;

                //going to disable the stop and clear buttons because we will be doing this work in the background
                SetButtonsState(false, false, false, false, false, false, false, false, false, false);
                TurnOnWaitingSpinner();
                raceEnded = true;
                disconnectGateways();
                Console.WriteLine("stopping the race");
                raceEndTime = DateTime.Now;

                //need to set the current races stop time with the end of the race time and update the race table
                RaceDAO raceDAO = new RaceDAO();
                //List<Race> races = raceDAO.GetRaceByRaceId(_raceId);
                //if (races.Count == 1)
                //{
                //Race race = races[0];
                Race race = getRace();
                race.RaceEndTime = raceEndTime.ToString();
                raceDAO.UpdateRace(race);
                //}
                UserDAO userDAO = new UserDAO();
                StatisticsDAO statisticsDAO = new StatisticsDAO();

                //need to save all of our runner lap information     
                foreach (var item in _raceInformation)
                {
                    string tagId = item.Key;
                    User currentUser = getUserFromUsersListByTagId(tagId);

                    List<DateTime> runnersLaps = item.Value;
                    int lapNumber = 0;
                    foreach(DateTime currentLapDateTime in runnersLaps)
                    {
                        
                        if(lapNumber == 0)
                        {
                            lapNumber++;
                            continue;
                        }
                        
                        //need to insert each lap as a user statistic
                        statisticsDAO.InsertUserStatistic(currentUser.Id, _raceId, lapNumber, currentLapDateTime);
                        lapNumber++;
                    }
                    currentUser.Laps = lapNumber - 1;
                    //need to set the user's laps to be equal to the currentLapNumber
                    userDAO.UpdateUser(currentUser);


                }

            };
            bgw.RunWorkerCompleted += (_, __) =>
            {
                TurnOffWaitingSpinner();
                Dispatcher.Invoke(() =>
                {
                    LoadRaceInformation();
                });

            };
            bgw.RunWorkerAsync();
        }
        private void disconnectGateways()
        {
            try
            {
                foreach (var tagScanner in _tagScanners)
                {
                    try
                    {
                        vtagmanager.VTagManager.Instance.RemoveGateway(tagScanner.TagScannerIp.ToString());
                    }
                    catch(Exception exc)
                    {
                        Console.WriteLine("llrp was already disconnected");
                    }
                }
            }
            catch(Exception exc)
            {
                Console.WriteLine("LLRP not connected");
            }

        }
       
        private void SetButtonsState(bool goToRacesPageButtonState, bool addScannerButtonState, bool deleteScannerState, bool startRaceButtonState, bool stopRaceButtonState, bool clearRaceButtonState, bool importUsersButtonState, bool addUserButtonState, bool deleteUserButtonState, bool editUserButtonState)
        {
            
            Dispatcher.Invoke(() =>
            {
                navigateToChooseRacesPageButton.IsEnabled = goToRacesPageButtonState;
                addTagScannersButton.IsEnabled = addScannerButtonState;
                deleteTagScannerButton.IsEnabled = deleteScannerState;

                startRaceButton.IsEnabled = startRaceButtonState;
                stopRaceButton.IsEnabled = stopRaceButtonState;
                clearRaceButton.IsEnabled = clearRaceButtonState;

                importButton.IsEnabled = importUsersButtonState;
                addUserButton.IsEnabled = addUserButtonState;
                removeUserButton.IsEnabled = deleteUserButtonState;
                editUserButton.IsEnabled = editUserButtonState; 
            });
            
        }
        private void dgUsers_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }
        private User getUserFromUsersListByTagId(string tagId)
        {
            foreach(User currentUser in _users)
            {
                if (currentUser.TagId.Equals(tagId))
                {
                    return currentUser;
                }
            }
            return null;
        }
    }
    
}
