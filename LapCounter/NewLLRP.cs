﻿using Org.LLRP.LTK.LLRPV1;
using Org.LLRP.LTK.LLRPV1.DataType;
using System;

namespace RaceTracker
{
    public class NewLLRP : IReader
    {
        LLRPClient reader;
        private readonly string ipAddress;

        public NewLLRP(string ipAddress)
        {
            reader = new LLRPClient();
            this.ipAddress = ipAddress;
        }

        public void Connect()
        {
            ENUM_ConnectionAttemptStatusType status;
            reader.Open(ipAddress, 2000, out status);
            if (status != ENUM_ConnectionAttemptStatusType.Success)
            {
                Console.WriteLine("Could not connect: " + status.ToString());
                Connected?.Invoke(this, false, "Could not connect.");
                return;
            }
            else
            {
                Connected?.Invoke(this, true, string.Empty);
            }
        }

        public void Disconnect()
        {
            reader.Close();
        }

        public void SetGPO1(bool high, int seconds)
        {
            throw new NotImplementedException();
        }

        public void SetGPO2(bool high, int seconds)
        {
            throw new NotImplementedException();
        }

        public void StartAutoMode()
        {
            reader.OnRoAccessReportReceived += OnReportEvent;
            Delete_RoSpec();
            Add_RoSpec();
            Enable_RoSpec();
        }

        public void StopAutoMode()
        {
            reader.OnRoAccessReportReceived -= OnReportEvent;
            Delete_RoSpec();
        }

        void OnReportEvent(MSG_RO_ACCESS_REPORT msg)
        {
            if (msg.TagReportData == null || msg.TagReportData.Length == 0)
            {
                Console.WriteLine("OnReportEvent - No Tag Data");
                return;
            }
            for (int i = 0; i < msg.TagReportData.Length; i++)
            {
                if (msg.TagReportData[i].EPCParameter.Count > 0)
                {
                    string epc;
                    if (msg.TagReportData[i].EPCParameter[0].GetType() == typeof(PARAM_EPC_96))
                    {
                        epc = ((PARAM_EPC_96)msg.TagReportData[i].EPCParameter[0]).EPC.ToHexString();
                    }
                    else
                    {
                        epc = ((PARAM_EPCData)msg.TagReportData[i].EPCParameter[0]).EPC.ToHexString();
                    }
                    NewTags?.Invoke(this, new TagObservation[] { new TagObservation() { TagID = epc } });
                }
                else
                    Console.WriteLine("OnReportEvent - No EPC data");
            }
        }

        void Delete_RoSpec()
        {
            MSG_DELETE_ROSPEC msg = new MSG_DELETE_ROSPEC();
            msg.ROSpecID = 0;
            MSG_ERROR_MESSAGE msg_err;
            MSG_DELETE_ROSPEC_RESPONSE rsp = reader.DELETE_ROSPEC(msg, out msg_err, 2000);
            if (rsp != null)
            {
                Console.WriteLine("Deleted RoSpec");
            }
            else if (msg_err != null)
            {
                Console.WriteLine("Got Delet RoSpec err");
            }
            else
            {
                Console.WriteLine("Got Delet RoSpec timeout err");
            }
        }

        void Add_RoSpec()
        {
            PARAM_C1G2InventoryCommand c1g2Inv = new PARAM_C1G2InventoryCommand();
            PARAM_C1G2RFControl c1g2RF = new PARAM_C1G2RFControl();
            // Set the reader mode to one of these values.
            // 0 = Max Throughput
            // 1 = Hybrid
            // 2 = Dense Reader M4
            // 3 = Dense Reader M8
            // 4 = Max Miller
            // 1000 = Autoset Dense Reader
            // 1001 = Autoset Single Reader
            c1g2RF.ModeIndex = 2;
            c1g2RF.Tari = 0;
            c1g2Inv.C1G2RFControl = c1g2RF;
            // Set the session.
            PARAM_C1G2SingulationControl c1g2Sing = new PARAM_C1G2SingulationControl();
            c1g2Sing.Session = new TwoBits(1);
            c1g2Sing.TagPopulation = 32;
            c1g2Sing.TagTransitTime = 0;
            c1g2Inv.C1G2SingulationControl = c1g2Sing;
            c1g2Inv.TagInventoryStateAware = false;




            MSG_ERROR_MESSAGE msg_err;
            MSG_ADD_ROSPEC msg = new MSG_ADD_ROSPEC();
            msg.ROSpec = new PARAM_ROSpec();
            msg.ROSpec.CurrentState = ENUM_ROSpecState.Disabled;
            msg.ROSpec.ROSpecID = 123;

            msg.ROSpec.ROBoundarySpec = new PARAM_ROBoundarySpec();
            msg.ROSpec.ROBoundarySpec.ROSpecStartTrigger = new PARAM_ROSpecStartTrigger();
            msg.ROSpec.ROBoundarySpec.ROSpecStartTrigger.ROSpecStartTriggerType = ENUM_ROSpecStartTriggerType.Immediate;
            msg.ROSpec.ROBoundarySpec.ROSpecStopTrigger = new PARAM_ROSpecStopTrigger();
            msg.ROSpec.ROBoundarySpec.ROSpecStopTrigger.ROSpecStopTriggerType = ENUM_ROSpecStopTriggerType.Null;

            msg.ROSpec.SpecParameter = new UNION_SpecParameter();
            PARAM_AISpec aiSpec = new PARAM_AISpec();
            aiSpec.AntennaIDs = new UInt16Array();
            aiSpec.AntennaIDs.Add(0);
            aiSpec.AISpecStopTrigger = new PARAM_AISpecStopTrigger();
            aiSpec.AISpecStopTrigger.AISpecStopTriggerType = ENUM_AISpecStopTriggerType.Null;
            aiSpec.InventoryParameterSpec = new PARAM_InventoryParameterSpec[1];
            aiSpec.InventoryParameterSpec[0] = new PARAM_InventoryParameterSpec();
            aiSpec.InventoryParameterSpec[0].InventoryParameterSpecID = 1234;
            aiSpec.InventoryParameterSpec[0].ProtocolID = ENUM_AirProtocols.EPCGlobalClass1Gen2;
            //new thing
            aiSpec.InventoryParameterSpec[0].AntennaConfiguration = new PARAM_AntennaConfiguration[1];
            aiSpec.InventoryParameterSpec[0].AntennaConfiguration[0] = new PARAM_AntennaConfiguration();
            aiSpec.InventoryParameterSpec[0].AntennaConfiguration[0].AntennaID = 0;
            aiSpec.InventoryParameterSpec[0].AntennaConfiguration[0].AirProtocolInventoryCommandSettings.Add(c1g2Inv);

            msg.ROSpec.SpecParameter.Add(aiSpec);

            msg.ROSpec.ROReportSpec = new PARAM_ROReportSpec();
            msg.ROSpec.ROReportSpec.ROReportTrigger = ENUM_ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec;
            msg.ROSpec.ROReportSpec.N = 1;
            msg.ROSpec.ROReportSpec.TagReportContentSelector = new PARAM_TagReportContentSelector();
            MSG_ADD_ROSPEC_RESPONSE rsp = reader.ADD_ROSPEC(msg, out msg_err, 2000);
            if (rsp != null)
            {
                Console.WriteLine("Added RoSpec");
            }
            else if (msg_err != null)
            {
                Console.WriteLine("Got Add RoSpec err");
            }
            else
            {
                Console.WriteLine("Got Add RoSpec timeout err");
            }
        }

        void Enable_RoSpec()
        {
            MSG_ERROR_MESSAGE msg_err;
            MSG_ENABLE_ROSPEC msg = new MSG_ENABLE_ROSPEC();
            msg.ROSpecID = 123;
            MSG_ENABLE_ROSPEC_RESPONSE rsp = reader.ENABLE_ROSPEC(msg, out msg_err, 2000);
            if (rsp != null)
            {
                Console.WriteLine("Enabled RoSpec");
            }
            else if (msg_err != null)
            {
                Console.WriteLine("Got Enabled RoSpec err");
            }
            else
            {
                Console.WriteLine("Got Enabled RoSpec timeout err");
            }
        }

        public long ID => 1;
        public string Name => "Whateva";
        public string Description => "Go away";
        public event NewTagsDelegate NewTags;
        public event DisconnectedDelegate Disconnected;
        public event ConnectedDelegate Connected;

    }
}
