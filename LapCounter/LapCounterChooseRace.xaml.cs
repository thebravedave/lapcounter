﻿using LapCounter.dal;
using LapCounter.dto;
using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LapCounter
{

    public partial class LapCounterChooseRace : Page
    {
        private List<Race> races = new List<Race>();

        public LapCounterChooseRace()
        {
            DatabaseManager databaseManager = new DatabaseManager();
            databaseManager.PrepareDatabase();
            InitializeComponent();
            InitializePage();
            LoadRaces();
            LoadAdministrators();
        }
        public void AddAdministrator(object sender, RoutedEventArgs e)
        {
            AddAdministrator addAdministrator = new AddAdministrator();
            this.NavigationService.Navigate(addAdministrator);
        }
        public void DeleteAdministrator(object sender, RoutedEventArgs e)
        {
            Administrator administrator = (Administrator)dgAdministrators.SelectedItem;
            AdministratorDAO administratorDAO = new AdministratorDAO();
            administratorDAO.DeleteAdministrator(administrator.Id);
            LoadAdministrators();
            
        }
        public void AddRace(object sender, RoutedEventArgs e)
        {
            if(this.raceName.Text.Equals("") || this.raceDate.Text.Equals(""))
            {
                return;
            }

            string raceName = this.raceName.Text;
            string raceDate = this.raceDate.Text;
            string[] dateArray = raceDate.Split('/');
            raceDate = dateArray[2] + "/" + dateArray[0] + "/" + dateArray[1];
            RaceDAO raceDAO = new RaceDAO();
            bool raceAdded = raceDAO.AddRace(raceName, raceDate);
            LoadRaces();
        }
        public void EditRaceButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                Race race = (Race)dgRaces.SelectedItem;              
                EditRace editRace = new EditRace(race.Id);
                this.NavigationService.Navigate(editRace);
            }
            catch(Exception ex)
            {
                Console.WriteLine("error: " + ex.Message);
            }
        }
        public void DeleteRace(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageManager messageManager = new MessageManager();
                bool result = messageManager.ShowChallengeMessage("Delete The Whole Race", "Are you sure you want to delete the race?  This will delete all data associated with this race and can't be undone.  Click 'no' to not delete this race.");
                if (!result)
                {
                    return;
                }
                Race race = (Race)dgRaces.SelectedItem;
                RaceDAO raceDAO = new RaceDAO();
                raceDAO.DeleteRace(race.Id);

                //need to delete the statistics
                StatisticsDAO statisticsDAO = new StatisticsDAO();
                statisticsDAO.DeleteStatisticsForRace(race.Id);

                //need to delete the users
                UserDAO userDAO = new UserDAO();
                ObservableCollection<User> users = userDAO.FetchAllUsers(race.Id);
                foreach(User user in users)
                {
                    userDAO.DeleteUser(user.Id, race.Id);
                }

                //need to delete the tagscanner
                ScannerDAO scannerDAO = new ScannerDAO();
                List<TagScanner> scanners = scannerDAO.GetTagScannersForRace(race.Id);
                foreach(TagScanner tagScanner in scanners)
                {
                    scannerDAO.DeleteScanner(tagScanner.Id, race.Id);
                }
                LoadRaces();
            }
            catch(Exception ex)
            {
                return;
            }
        }
        public void GoToRace(object sender, RoutedEventArgs e)
        {
            try
            {
                //need to pass the Race object to the LapCounterHome page
                Race race = (Race)dgRaces.SelectedItem;

                LapCounterHome editUserPage = new LapCounterHome(race.Id);
                this.NavigationService.Navigate(editUserPage, race);
            }
            catch(Exception exc)
            {
                Console.WriteLine("test");
            }

        }
        public void InitializePage()
        {
            raceDate.DisplayDateStart = DateTime.Now;
        }
        public void LoadRaces()
        {
            RaceDAO raceDAO = new RaceDAO();
            races = raceDAO.GetRaces();
            dgRaces.ItemsSource = races;
        }
        public void LoadAdministrators()
        {
            AdministratorDAO administratorDAO = new AdministratorDAO();
            List<Administrator> administrators = administratorDAO.GetAllAdministrators();
            dgAdministrators.ItemsSource = administrators;
        }
    }
}
