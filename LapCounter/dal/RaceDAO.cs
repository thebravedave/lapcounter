﻿using LapCounter.dto;
using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dal
{
    class RaceDAO
    {
        

        public List<Race> races = new List<Race>();
       
        public bool CheckIfRaceExists(string raceName, string raceDate)
        {
            List<Race> races = GetRaceByRaceNameAndDate(raceName, raceDate);
            if(races.Count == 0)
            {
                return false;
            }
            return true;
        }
        public bool AddRace(string raceName, string raceDate)
        {
            if (CheckIfRaceExists(raceName, raceDate))
            {
                return false;
            }
            int result = -1;

            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";

            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                //using (SQLiteConnection conn = DBConnectionManager.GetConnection(connectionString))
                {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "INSERT INTO races(racename, racedate) VALUES(@racename,  @racedate)";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@racename", raceName);
                    cmd.Parameters.AddWithValue("@racedate", raceDate);

                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        public Boolean DeleteRace(int raceId)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "DELETE FROM races WHERE id=@id";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@id", raceId);
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        public List<Race> GetRaceByRaceId(int raceId)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<Race> races = new List<Race>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                    //using (SQLiteConnection conn = DBConnectionManager.GetConnection(connectionString))
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(conn))
                        {
                            cmd.CommandText = "SELECT * FROM races " +
                            "WHERE id=@raceid";
                            cmd.Prepare();
                            cmd.Parameters.AddWithValue("@raceid", raceId);


                            using (SQLiteDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    Race currentRace = new Race();
                                    currentRace.Id = Int32.Parse(reader["Id"].ToString());
                                    currentRace.RaceName = reader["racename"].ToString();
                                    currentRace.RaceDate = reader["racedate"].ToString();
                                    currentRace.RaceStartTime = reader["racestarttime"].ToString();
                                    currentRace.RaceEndTime = reader["raceendtime"].ToString();
                                    races.Add(currentRace);
                                }
                            }
                        }
                        conn.Close();
                    }
                }
            }
            catch (SQLiteException e)
            {
                Console.WriteLine("error");
            }
            return races;
        }
        public List<Race> GetRaceByRaceNameAndDate(string raceName, string raceDate)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<Race> races = new List<Race>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                    //using (SQLiteConnection conn = DBConnectionManager.GetConnection(connectionString))
                    {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM races " +
                        "WHERE racename=@racename " +
                        "AND racedate=@racedate";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@racename", raceName);
                        cmd.Parameters.AddWithValue("@racedate", raceDate);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Race currentRace = new Race();
                                currentRace.RaceName = reader["racename"].ToString();
                                currentRace.RaceDate = reader["racedate"].ToString();
                                currentRace.RaceStartTime = reader["racestarttime"].ToString();
                                currentRace.RaceEndTime = reader["raceendtime"].ToString();

                                races.Add(currentRace);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                Console.WriteLine("error");
            }
            return races;
        }
        public List<Race> GetRaces()
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<Race> races = new List<Race>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                    //using (SQLiteConnection conn = DBConnectionManager.GetConnection(connectionString))
                    {
                    string sql = "SELECT * FROM races ORDER BY racedate ASC";
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                    {
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Race currentRace = new Race();
                                currentRace.Id = Int32.Parse(reader["id"].ToString());
                                currentRace.RaceName = reader["racename"].ToString();
                                currentRace.RaceDate = reader["racedate"].ToString();
                                races.Add(currentRace);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                Console.WriteLine("test");
            }
            return races;
        }
        public bool UpdateRace(Race race)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                //using (SQLiteConnection conn = DBConnectionManager.GetConnection(connectionString))
                {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "UPDATE races " +
                        "SET racename=@racename, " +
                        "racedate=@racedate, " +
                        "racestarttime=@racestarttime, " +
                        "raceendtime=@raceendtime " +
                        "WHERE id=@id";

                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@racename", race.RaceName);
                    cmd.Parameters.AddWithValue("@racedate", race.RaceDate);
                    cmd.Parameters.AddWithValue("@racestarttime", race.RaceStartTime);
                    cmd.Parameters.AddWithValue("@raceendtime", race.RaceEndTime);
                    cmd.Parameters.AddWithValue("@id", race.Id);

                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        Console.WriteLine("end update race");
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
    }
}
