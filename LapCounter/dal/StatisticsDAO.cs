﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LapCounter.dto;
using LapCounter.managers;

namespace LapCounter.dal
{
    class StatisticsDAO
    {

        public bool InsertUserStatistic(int userId, int raceId, int lapNumber, DateTime dateTime)
        {

            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        string utcTime = dateTime.ToString();
                        cmd.CommandText = "INSERT INTO userstatistics(userid, raceid, lapnumber, laptime) VALUES(@userid, @raceid, @lapnumber, @laptime)";

                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@userid", userId);
                        cmd.Parameters.AddWithValue("@raceid", raceId);
                        cmd.Parameters.AddWithValue("@lapnumber", lapNumber);
                        cmd.Parameters.AddWithValue("@laptime", utcTime);

                        try
                        {
                            result = cmd.ExecuteNonQuery();
                            if (result == 0)
                            {
                                return false;
                            }
                        }
                        catch (SQLiteException e)
                        {
                            return false;
                        }
                    }
                    conn.Close();
                }
            }
            return true;
        }
        public bool InsertUserStatistics(int userId, int raceId, int lapNumber)
        {

            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    string utcTime = DateTime.Now.ToString();
                    cmd.CommandText = "INSERT INTO userstatistics(userid, raceid, lapnumber, laptime) VALUES(@userid, @raceid, @lapnumber, @laptime)";
                        
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@userid", userId);
                    cmd.Parameters.AddWithValue("@raceid", raceId);
                    cmd.Parameters.AddWithValue("@lapnumber", lapNumber);
                    cmd.Parameters.AddWithValue("@laptime", utcTime);

                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        internal List<UserStatistic> GetUserStatisticsByUserId(int userId)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<UserStatistic> userStatistics = new List<UserStatistic>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM userstatistics " +
                        "WHERE userid=@userid ORDER BY lapnumber ASC";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@userid", userId);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UserStatistic currentUser = new UserStatistic();
                                currentUser.Id = Int32.Parse(reader["id"].ToString());
                                currentUser.UserId = Int32.Parse(reader["userid"].ToString());
                                currentUser.RaceId = Int32.Parse(reader["raceid"].ToString());
                                currentUser.LapNumber = Int32.Parse(reader["lapnumber"].ToString());
                                currentUser.LapTime = reader["laptime"].ToString();
                                userStatistics.Add(currentUser);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                return userStatistics;
            }
            return userStatistics;
        }
        internal bool DeleteStatisticsForRace(int raceId)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "DELETE FROM userstatistics WHERE raceid=@raceid";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@raceid", raceId);
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
    }
}
