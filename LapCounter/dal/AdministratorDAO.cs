﻿using LapCounter.dto;
using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace LapCounter.dal
{
    class AdministratorDAO
    {
        

        public bool CheckIfAdminExists(string administratorName, string administratorEmail)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<Administrator> administrators = new List<Administrator>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM administrators " +
                        "WHERE administratorname=@administratorname " +
                        "AND administratoremail=@administratoremail";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@administratorname", administratorName);
                        cmd.Parameters.AddWithValue("@administratoremail", administratorEmail);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Administrator currentAdministrator = new Administrator();
                                currentAdministrator.Id = Int32.Parse(reader["id"].ToString());
                                currentAdministrator.AdministratorName = reader["administratorname"].ToString();
                                currentAdministrator.AdministratorEmail = reader["administratoremail"].ToString();

                                administrators.Add(currentAdministrator);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                return true;
            }
            if (administrators.Count == 0)
            {
                return false;
            }
            return true;
        }
        public bool AddAdministrator(Administrator administrator)
        {
            if (administrator == null || administrator.AdministratorName.Equals("") || administrator.AdministratorEmail.Equals("") || CheckIfAdminExists(administrator.AdministratorName, administrator.AdministratorEmail))
            {
                return false;
            }
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "INSERT INTO administrators(administratorname, administratoremail) VALUES(@administratorname, @administratoremail)";

                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@administratorname", administrator.AdministratorName);
                    cmd.Parameters.AddWithValue("@administratoremail", administrator.AdministratorEmail);

                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        public bool DeleteAdministrator(int adminId)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "DELETE FROM administrators WHERE id=@id";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@id", adminId);
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        internal List<Administrator> GetAllAdministrators()
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<Administrator> administrators = new List<Administrator>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM administrators ORDER BY administratorname ASC";
                        cmd.Prepare();

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Administrator currentAdministrator = new Administrator();
                                currentAdministrator.Id = Int32.Parse(reader["id"].ToString());
                                currentAdministrator.AdministratorName = reader["administratorname"].ToString();
                                currentAdministrator.AdministratorEmail = reader["administratoremail"].ToString();
                                administrators.Add(currentAdministrator);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                Console.WriteLine("test");
            }
            return administrators;
        }
    }
}
