﻿using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dal
{
    class DatabaseManager
    {
        

        public bool PrepareDatabase()
        {
            SQLiteConnection sqlite2;
            try
            {
                if (!System.IO.File.Exists(LocationManager.DatabaseLocation + "\\LapCounter.sqlite"))
                {
                    if (!string.IsNullOrEmpty(LocationManager.DatabaseLocation) && !Directory.Exists(LocationManager.DatabaseLocation))
                    {
                        Directory.CreateDirectory(LocationManager.DatabaseLocation);
                    }
                     

                    SQLiteConnection.CreateFile(LocationManager.DatabaseLocation + "\\LapCounter.sqlite");
                    string connectionString = "Data Source=" + LocationManager.DatabaseLocation  + "\\LapCounter.sqlite";
                    using (sqlite2 = ConnectionManager.CreateConnection(connectionString))
                    {
                        string sql = "create table IF NOT EXISTS administrators(id INTEGER PRIMARY KEY, administratorname varchar(32), administratoremail varchar(32))";
                        SQLiteCommand command = new SQLiteCommand(sql, sqlite2);
                        command.ExecuteNonQuery();
                    }
                    using (sqlite2 = ConnectionManager.CreateConnection(connectionString))
                    {
                        string sql = "create table IF NOT EXISTS tagscanners(id INTEGER PRIMARY KEY, raceid int, tagscannerip varchar(32))";
                        SQLiteCommand command = new SQLiteCommand(sql, sqlite2);
                        command.ExecuteNonQuery();
                    }
                    using (sqlite2 = ConnectionManager.CreateConnection(connectionString))
                    {
                        string sql = "create table IF NOT EXISTS users (id INTEGER PRIMARY KEY, runnerlastname varchar(40), runnerfirstname varchar(40), runnergrade int, parentlastname varchar(40), parentfirstname varchar(40), raceid int, parentname varchar(20), email varchar(128), laps int, vestnumber varchar(22), tagid varchar (32))";
                        SQLiteCommand command = new SQLiteCommand(sql, sqlite2);
                        command.ExecuteNonQuery();
                    }

                    using (sqlite2 = ConnectionManager.CreateConnection(connectionString))
                    {
                        string sql = "create table IF NOT EXISTS races (id INTEGER PRIMARY KEY, racename varchar(120), racedate varchar(100), racestarttime varchar(100), raceendtime varchar(100))";
                        SQLiteCommand command = new SQLiteCommand(sql, sqlite2);
                        command.ExecuteNonQuery();
                    }

                    using (sqlite2 = ConnectionManager.CreateConnection(connectionString))
                    {
                        string sql = "create table IF NOT EXISTS userstatistics (id INTEGER PRIMARY KEY, userid int, raceid int, lapnumber int, laptime varchar(100))";
                        SQLiteCommand command = new SQLiteCommand(sql, sqlite2);
                        command.ExecuteNonQuery();
                    }

                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
