﻿using LapCounter.dto;
using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dal
{
    class UserDAO
    {


        public void UpdateUser(User user)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "UPDATE users " +
                            "SET runnerfirstname=@runnerfirstname, " +
                            "runnerlastname=@runnerlastname, " +
                            "parentlastname=@parentlastname, " +
                            "parentfirstname=@parentfirstname, " +
                            "runnergrade=@runnergrade, " +
                            "email=@email, " +
                            "laps=@laps, " +
                            "vestnumber=@vestnumber, " +
                            "tagid=@tagid " +
                            "WHERE id=@id";

                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@runnerfirstname", user.RunnerFirstName);
                        cmd.Parameters.AddWithValue("@runnerlastname", user.RunnerLastName);
                        cmd.Parameters.AddWithValue("@parentlastname", user.ParentLastName);
                        cmd.Parameters.AddWithValue("@parentfirstname", user.ParentFirstName);
                        cmd.Parameters.AddWithValue("@runnergrade", user.RunnerGrade);
                        cmd.Parameters.AddWithValue("@email", user.Email);
                        cmd.Parameters.AddWithValue("@laps", user.Laps);
                        cmd.Parameters.AddWithValue("@vestnumber", user.VestNumber);
                        cmd.Parameters.AddWithValue("@tagid", user.TagId);
                        cmd.Parameters.AddWithValue("@id", user.Id);

                        try
                        {
                            result = cmd.ExecuteNonQuery();
                        }
                        catch (SQLiteException e)
                        {
                            Console.WriteLine("test");
                        }
                    }
                    conn.Close();
                }
            }
        }
        public bool CheckIfUserExists(int raceId, string firstName, string lastName)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<User> users = new List<User>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM users " +
                        "WHERE raceid=@raceid " +
                        "AND runnerfirstname=@runnerfirstname " +
                        "AND runnerlastname=@runnerlastname";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@raceid", raceId);
                        cmd.Parameters.AddWithValue("@runnerfirstname", firstName);
                        cmd.Parameters.AddWithValue("@runnerlastname", lastName);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                User currentUser = new User();
                                currentUser.Id = Int32.Parse(reader["id"].ToString());
                                currentUser.RunnerFirstName = reader["runnerfirstname"].ToString();
                                currentUser.RunnerLastName = reader["runnerlastname"].ToString();
                                currentUser.RunnerGrade = Int32.Parse(reader["runnergrade"].ToString());
                                currentUser.ParentFirstName = reader["parentfirstname"].ToString();
                                currentUser.ParentLastName = reader["parentlastname"].ToString();
                                currentUser.Email = reader["email"].ToString();
                                currentUser.Laps = Int32.Parse(reader["laps"].ToString());
                                currentUser.VestNumber = reader["vestnumber"].ToString();
                                currentUser.TagId = reader["tagid"].ToString();
                                users.Add(currentUser);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                return true;
            }
            if(users.Count == 0)
            {
                return false;
            }
            return true;          
        }
        public bool DeleteUser(int userId, int raceId)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "DELETE FROM users WHERE id=@id AND raceid=@raceid";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@id", userId);
                        cmd.Parameters.AddWithValue("@raceid", raceId);
                        try
                        {
                            result = cmd.ExecuteNonQuery();
                            if (result == 0)
                            {
                                return false;
                            }
                        }
                        catch (SQLiteException e)
                        {
                            return false;
                        }
                    }
                    conn.Close();
                }
            }
            if (raceId.Equals(-1))
            {
                return false;
            }
            return true;
        }
        public bool AddUser(int raceId, string runnerLastName, string runnerFirstName, int runnerGrade, string parentLastName, string parentFirstName, string email, string vestNumber, string tagId)
        {
            if (runnerFirstName.Equals("") || runnerLastName.Equals("") || email.Equals("") || CheckIfUserExists(raceId, runnerFirstName, runnerLastName))
            {
                return false;
            }
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "INSERT INTO users(raceid, runnerfirstname, runnerlastname, runnergrade, parentfirstname, parentlastname, email, laps, vestnumber, tagId) VALUES(@raceid, @runnerfirstname, @runnerlastname, @runnergrade, @parentfirstname, @parentlastname, @email, @laps, @vestnumber, @tagid)";

                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@raceid", raceId);
                    cmd.Parameters.AddWithValue("@runnerfirstname", runnerFirstName);
                    cmd.Parameters.AddWithValue("@runnerlastname", runnerLastName);
                    cmd.Parameters.AddWithValue("@runnergrade", runnerGrade);
                    cmd.Parameters.AddWithValue("@parentlastname", parentLastName);
                    cmd.Parameters.AddWithValue("@parentfirstname", parentFirstName);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@laps", 0);
                    cmd.Parameters.AddWithValue("@vestnumber", vestNumber);
                    cmd.Parameters.AddWithValue("@tagid", tagId);
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        public ObservableCollection<User> GetUsersByRaceId(int raceId)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            ObservableCollection<User> users = new ObservableCollection<User>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(conn))
                        {
                            cmd.CommandText = "SELECT * FROM users " +
                            "WHERE raceid=@raceid";
                            cmd.Prepare();
                            cmd.Parameters.AddWithValue("@raceid", raceId);

                            using (SQLiteDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    User currentUser = new User();
                                    currentUser.Id = Int32.Parse(reader["id"].ToString());
                                    currentUser.RunnerFirstName = reader["runnerfirstname"].ToString();
                                    currentUser.RunnerLastName = reader["runnerlastname"].ToString();
                                    currentUser.RunnerGrade = Int32.Parse(reader["runnergrade"].ToString());
                                    currentUser.ParentFirstName = reader["parentfirstname"].ToString();
                                    currentUser.ParentLastName = reader["parentlastname"].ToString();
                                    currentUser.Email = reader["email"].ToString();
                                    currentUser.Laps = Int32.Parse(reader["laps"].ToString());
                                    currentUser.VestNumber = reader["vestnumber"].ToString();
                                    currentUser.TagId = reader["tagid"].ToString();
                                    users.Add(currentUser);
                                }
                            }
                        }
                        conn.Close();
                    }
                }
            }
            catch (SQLiteException e)
            {
                return users;
            }
            return users;
        }
        public ObservableCollection<User> GetUsersByUserId(int raceId, int userId)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            ObservableCollection<User> users = new ObservableCollection<User>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM users " +
                        "WHERE id=@id AND raceid=@raceid";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@id", userId);
                        cmd.Parameters.AddWithValue("@raceid", raceId);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                User currentUser = new User();
                                currentUser.Id = Int32.Parse(reader["id"].ToString());
                                currentUser.RunnerFirstName = reader["runnerfirstname"].ToString();
                                currentUser.RunnerLastName = reader["runnerlastname"].ToString();
                                currentUser.RunnerGrade = Int32.Parse(reader["runnergrade"].ToString());
                                currentUser.ParentLastName = reader["parentlastname"].ToString();
                                currentUser.ParentFirstName = reader["parentfirstname"].ToString();
                                currentUser.Email = reader["email"].ToString();
                                currentUser.Laps = Int32.Parse(reader["laps"].ToString());
                                currentUser.VestNumber = reader["vestnumber"].ToString();
                                currentUser.TagId = reader["tagid"].ToString();
                                users.Add(currentUser);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                return users;
            }
            return users;
        }
        /*
        public ObservableCollection<User> GetUserByRaceAndTagId(int raceId,string tagId)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            ObservableCollection<User> users = new ObservableCollection<User>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM users " +
                        "WHERE tagid=@tagid " +
                        "AND raceid=@raceid";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@tagid", tagId);
                        cmd.Parameters.AddWithValue("@raceid", raceId);
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                User currentUser = new User();
                                currentUser.Id = Int32.Parse(reader["id"].ToString());
                                currentUser.RunnerFirstName = reader["runnerfirstname"].ToString();
                                currentUser.RunnerLastName = reader["runnerlastname"].ToString();
                                currentUser.RunnerGrade = Int32.Parse(reader["runnergrade"].ToString());
                                currentUser.ParentLastName = reader["parentlastname"].ToString();
                                currentUser.ParentFirstName = reader["parentfirstname"].ToString();
                                currentUser.Email = reader["email"].ToString();
                                currentUser.Laps = Int32.Parse(reader["laps"].ToString());
                                currentUser.VestNumber = reader["vestnumber"].ToString();
                                currentUser.TagId = reader["tagid"].ToString();
                                users.Add(currentUser);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                return users;
            }
            return users;
        }
        */
        public ObservableCollection<User> FetchAllUsers(int raceId)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            ObservableCollection<User> users = new ObservableCollection<User>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM users WHERE raceid=@raceid ORDER BY runnerlastname ASC";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@raceid", raceId);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                User currentUser = new User();
                                currentUser.Id = Int32.Parse(reader["id"].ToString());
                                currentUser.RunnerFirstName = reader["runnerfirstname"].ToString();
                                currentUser.RunnerLastName = reader["runnerlastname"].ToString();
                                currentUser.RunnerGrade = Int32.Parse(reader["runnergrade"].ToString());
                                currentUser.ParentLastName = reader["parentlastname"].ToString();
                                currentUser.ParentFirstName = reader["parentfirstname"].ToString();
                                currentUser.Email = reader["email"].ToString();
                                currentUser.Laps = Int32.Parse(reader["laps"].ToString());
                                currentUser.VestNumber = reader["vestnumber"].ToString();
                                currentUser.TagId = reader["tagid"].ToString();
                                users.Add(currentUser);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("test");
            }
            return users;
        }
    }
}
