﻿using LapCounter.dto;
using LapCounter.managers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapCounter.dal
{
    class ScannerDAO
    {
        
        private static object instanceLock;

        public List<TagScanner> GetTagScannersForRace(int raceId)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<TagScanner> scanners = new List<TagScanner>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(conn))
                        {
                            cmd.CommandText = "SELECT * FROM tagscanners " +
                            "WHERE raceid=@raceid";
                            cmd.Prepare();
                            cmd.Parameters.AddWithValue("@raceid", raceId);

                            using (SQLiteDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    TagScanner currentScanner = new TagScanner();
                                    currentScanner.Id = Int32.Parse(reader["id"].ToString());
                                    currentScanner.TagScannerIp = reader["tagscannerip"].ToString();
                                    scanners.Add(currentScanner);
                                }
                            }
                        }
                        conn.Close();
                    }
                }
            }
            catch (SQLiteException e)
            {
                return scanners;
            }
            return scanners;
        }
        public bool AddTagScanner(int raceId, string tagScannerIp)
        {
            //need to check if this ip address already exists as a tag scanner for this raceId.  If so then return, if not insert
            if(CheckIfTagScannerExists(raceId, tagScannerIp))
            {
                return false;
            }
            //then this is a new ip address for a new scanner so we should add it to the database
            return InsertTagScanner(raceId, tagScannerIp);
        }
        private bool CheckIfTagScannerExists(int raceId, string tagScannerIp)
        {
            List<TagScanner> tagScanners = GetTagScannersByRaceIdAndTagScannerIp(raceId, tagScannerIp);
            if(tagScanners.Count > 0)
            {
                return true;
            }
            return false;
        }
        private List<TagScanner> GetTagScannersByRaceIdAndTagScannerIp(int raceId, string tagScannerIp)
        {
            List<TagScanner> scanners = new List<TagScanner>();
            try
            {
                string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM tagscanners " +
                        "WHERE raceid=@raceid " +
                        "AND tagscannerip=@tagscannerip";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@raceid", raceId);
                        cmd.Parameters.AddWithValue("@tagscannerip", tagScannerIp);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TagScanner currentScanner = new TagScanner();
                                currentScanner.RaceId = Int32.Parse(reader["raceid"].ToString());
                                currentScanner.TagScannerIp = reader["tagscannerip"].ToString();
                                scanners.Add(currentScanner);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                return scanners;
            }
            return scanners;
        }
        internal bool DeleteScanner(int id, int raceId)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation  + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "DELETE FROM tagscanners WHERE id=@id";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@id", id);
                        try
                        {
                            result = cmd.ExecuteNonQuery();
                            if (result == 0)
                            {
                                return false;
                            }
                        }
                        catch (SQLiteException e)
                        {
                            return false;
                        }
                    }
                    conn.Close();
                }
            }
            if (raceId.Equals(-1))
            {
                return false;
            }   
            return true;
        }
        /*
        public List<RegistrationScanner> GetRegistrationScannerForRace(int raceId)
        {
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            List<RegistrationScanner> scanners = new List<RegistrationScanner>();
            try
            {
                lock (ConnectionManager.instanceLock)
                {
                    using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "SELECT * FROM registrationscanners " +
                        "WHERE raceid=@raceid";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@raceid", raceId);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                RegistrationScanner currentScanner = new RegistrationScanner();
                                currentScanner.RaceId = Int32.Parse(reader["raceid"].ToString());
                                currentScanner.RegistrationScannerIp = reader["registrationscannerip"].ToString();
                                scanners.Add(currentScanner);
                            }
                        }
                    }
                    conn.Close();
                }
                }
            }
            catch (SQLiteException e)
            {
                return scanners;
            }
            return scanners;
        }
        */
        /*
        public bool CheckIfRegistrationScannerExist(int raceId)
        {
            List<RegistrationScanner> registrationScanners = new List<RegistrationScanner>();
            registrationScanners = GetRegistrationScannerForRace(raceId);
            if (registrationScanners.Count == 0)
            {
                return false;
            }
            return true;
        }
        */
        /*
        public bool UpdateRegistrationScanner(int raceId, string registrationScannerIpAddress)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "UPDATE registrationscanners " +
                        "SET registrationscannerip=@registrationscannerip " +
                        "WHERE raceid=@raceid";

                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@registrationscannerip", registrationScannerIpAddress);
                    cmd.Parameters.AddWithValue("@raceid", raceId);
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {
                        Console.WriteLine("test");
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        */
        private bool InsertTagScanner(int raceId, string tagScannerIpAddress)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "INSERT INTO tagscanners(raceid, tagscannerip) VALUES(@raceid, @tagscannerip)";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@raceid", raceId);
                    cmd.Parameters.AddWithValue("@tagscannerip", tagScannerIpAddress);
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        /*
        public bool InsertRegistrationScanners(int raceId, string registrationScannerIpAddress)
        {
            int result = -1;
            string connectionString = "Data Source=" + LocationManager.DatabaseLocation + "\\LapCounter.sqlite";
            lock (ConnectionManager.instanceLock)
            {
                using (SQLiteConnection conn = ConnectionManager.CreateConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "INSERT INTO registrationscanners(raceid, registrationscannerip) VALUES(@raceid, @registrationscannerip)";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@raceid", raceId);
                    cmd.Parameters.AddWithValue("@registrationscannerip", registrationScannerIpAddress);
                    try
                    {
                        result = cmd.ExecuteNonQuery();
                        if (result == 0)
                        {
                            return false;
                        }
                    }
                    catch (SQLiteException e)
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            }
            return true;
        }
        */
        /*
        public bool AddRegistrationScanner(int raceId, string registrationScannerIpAddress)
        {
            if (CheckIfRegistrationScannerExist(raceId))
            {
                //then the scanners exists for this race and we should update them and not insert
                return UpdateRegistrationScanner(raceId, registrationScannerIpAddress);
            }
           return InsertRegistrationScanners(raceId, registrationScannerIpAddress);        
        }
        */
    }
}
