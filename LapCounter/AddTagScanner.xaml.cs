﻿using LapCounter.dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LapCounter
{
    /// <summary>
    /// Interaction logic for AddTagScanner.xaml
    /// </summary>
    public partial class AddTagScanner : Page
    {
        private int _raceId;

        public AddTagScanner(int raceId)
        {
            _raceId = raceId;
            InitializeComponent();
        }
        public void TestIpAddress(object sender, RoutedEventArgs e)
        {
            bool pingable = false;
            Ping pinger = null;
            string ipAddress = this.tagScannerIp.Text;
            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(ipAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error pinging your scanner.");
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }
            if (pingable)
            {
                //then this internal ip address is reachable.
                this.pingLabel.Content = "The ip address is reachable";
            }
            else
            {
                //then this internal ip address is not reachable
                this.pingLabel.Content = "The ip address is not reachable.  This could be because the scanner is not connected to the same internal network as the Lap Counter software or that the scanner is broken.";

            }
            
        }
        public void BackToRaceHomeButtonClicked(object sender, RoutedEventArgs e)
        {
            LapCounterHome lapCountHome = new LapCounterHome(_raceId);
            this.NavigationService.Navigate(lapCountHome);
        }
        public void AddTagScannerButtonClicked(object sender, RoutedEventArgs e)
        {
            string tagScannerIp = this.tagScannerIp.Text.Trim();
            if(tagScannerIp == null || tagScannerIp.Equals(""))
            {
                return;
            }
            ScannerDAO scannerDAO = new ScannerDAO();
            scannerDAO.AddTagScanner(_raceId, tagScannerIp);

            LapCounterHome lapCounterHome = new LapCounterHome(_raceId);
            this.NavigationService.Navigate(lapCounterHome);

        }
    }
}
